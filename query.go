package wages

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/stjarni/wages/core/amount"
	"gitlab.com/stjarni/wages/core/timeutil"
)

// Query is an interface for db queries
type Query interface {
	ScanEmployees(ctx context.Context, p *Period) ([]*Employee, error)
	Employees(ctx context.Context, p *Period, ids ...int32) ([]*Employee, error)
	EmployeeShifts(ctx context.Context, p *Period, empls ...*Employee) (EmployeeShifts, error)
	CompanyPolicies(ctx context.Context, period *Period) ([]*CompanyPolicy, error)
	Periods(ctx context.Context, p *Period) ([]*WagesPeriod, error)
}

// EmployeeRevision contains employee attributes from 'Date' point in time to next revision
type EmployeeRevision struct {
	Date              time.Time                        `json:"revision"`
	Employee          *Employee                        `json:"-"`
	MonthySalary      *amount.DKK                      `json:"monthly_salary,omitempty"`
	SalaryAccount     *BankAccount                     `json:"salary_account,omitempty"`
	Pension           *PensionRates                    `json:"pension,omitempty"`
	PensionAccount    *BankAccount                     `json:"pension_account,omitempty"`
	UnionMembershipNo string                           `json:"union_membership_no,omitempty"`
	Union             *EmployeeUnionRevision           `json:"union,omitempty"`
	LocalDepartment   *EmployeeLocalDepartmentRevision `json:"local_department,omitempty"`
	Positions         EmployeePositions                `json:"positions"`
}

type deptask struct {
	dep, task int32
}

// EmployeePositions ...
type EmployeePositions map[deptask]*EmployeePosition

// MarshalJSON is json Marshaler impl
func (p *EmployeePositions) MarshalJSON() ([]byte, error) {
	var res []*EmployeePosition
	for _, v := range *p {
		res = append(res, v)
	}
	return json.Marshal(res)
}

// Position returns employee position for specified department & task
func (p EmployeePositions) Position(depID, taskID int32) *EmployeePosition {
	return p[deptask{dep: depID, task: taskID}]
}

// SetPosition sets position
func (p EmployeePositions) SetPosition(depID, taskID int32, pos *EmployeePosition) {
	p[deptask{dep: depID, task: taskID}] = pos
}

// EmployeePosition contains employee position attributes for the employee revision
type EmployeePosition struct {
	Department       *Department          `json:"department"`
	Task             *EmployeeTask        `json:"task"`
	SalaryScale      *EmployeeSalaryScale `json:"salary_scale"`
	SplitRatio       *amount.Percentage   `json:"split_ratio,omitempty"`
	LunchBreakPolicy *LunchBreakPolicy    `json:"lunchbreak_policy,omitempty"`
	Bonuses          []*Bonus             `json:"bonuses,omitempty"`
}

// EmployeeSalaryScale contains salary-scale attributes for the 'Date' point in time
type EmployeeSalaryScale struct {
	ID          int32            `json:"id"`
	Name        string           `json:"name"`
	Date        time.Time        `json:"revision"`
	SalaryType  SalaryType       `json:"type"`
	DefaultRate *amount.DKK      `json:"default_rate,omitempty"`
	Pension     *PensionRates    `json:"pension,omitempty"`
	Rates       SalaryScaleRates `json:"rates,omitempty"`
}

// EmployeeUnionRevision contains union attributes for the 'Date' point in time
type EmployeeUnionRevision struct {
	ID                      int32         `json:"id"`
	Name                    string        `json:"name"`
	Email                   string        `json:"email,omitempty"`
	Phone                   string        `json:"phone,omitempty"`
	Date                    time.Time     `json:"revision"`
	MembershipFee           *FeeRates     `json:"membership_fee,omitempty"`
	InsuranceFee            *FeeRates     `json:"insurance_fee,omitempty"`
	Pension                 *PensionRates `json:"pension,omitempty"`
	MembershipFeeAccount    *BankAccount  `json:"membership_fee_account,omitempty"`
	InsuranceFeeAccount     *BankAccount  `json:"insurance_fee_account,omitempty"`
	PensionAccount          *BankAccount  `json:"pension_account,omitempty"`
	HolidayAllowanceAccount *BankAccount  `json:"holiday_allowance_account,omitempty"`
}

// EmployeeLocalDepartmentRevision contains local department attributes for the 'Date' point in time
type EmployeeLocalDepartmentRevision struct {
	ID                      int32        `json:"id"`
	Name                    string       `json:"name"`
	Date                    time.Time    `json:"revision"`
	MembershipFee           *FeeRates    `json:"membership_fee,omitempty"`
	MembershipFeeAccount    *BankAccount `json:"membership_fee_account,omitempty"`
	HolidayAllowanceAccount *BankAccount `json:"holiday_allowance_account,omitempty"`
}

// Bonus struct
type Bonus struct {
	ID           string             `json:"id"`
	Text         string             `json:"text"`
	DepartmentID int32              `json:"-"`
	TaskID       int32              `json:"-"`
	StartDate    time.Time          `json:"date_start"`
	EndDate      time.Time          `json:"date_end,omitempty"`
	Type         SalaryType         `json:"type"`
	AmountType   amount.Amount      `json:"-"`
	Amount       *amount.DKK        `json:"amount_dkk,omitempty"`
	Percentage   *amount.Percentage `json:"amount_percentage,omitempty"`
}

// Department struct
type Department struct {
	ID             int32       `json:"id"`
	Name           string      `json:"name"`
	HeadDepartment *Department `json:"head_department,omitempty"`
}

// EmployeeTask struct
type EmployeeTask struct {
	ID   int32  `json:"id"`
	Name string `json:"name"`
}

// BankAccount contains account in bank (/w bank name)
type BankAccount struct {
	RegNo     string `json:"regno"`
	AccountNo string `json:"accountno"`
	BankName  string `json:"bank"`
}

func (a *BankAccount) String() string {
	if a == nil {
		return ""
	}
	return fmt.Sprintf("%s-%s %s", a.RegNo, a.AccountNo, a.BankName)
}

// MarshalJSON implements json Marshaler
func (a *BankAccount) MarshalJSON() ([]byte, error) {
	return json.Marshal(a.String())
}

// FeeRates contains rates for union / local department fees
type FeeRates struct {
	RateByEmployer *amount.Percentage `json:"by_employer,omitempty"`
	RateByEmployee *amount.Percentage `json:"by_employee,omitempty"`
	// Fee Maximum. Zero value represents unbounded fee
	Maximum *amount.DKK `json:"maximum,omitempty"`
}

// PensionRates contains pension rates
type PensionRates struct {
	RateByEmployer *amount.Percentage `json:"by_employer,omitempty"`
	RateByEmployee *amount.Percentage `json:"by_employee,omitempty"`
}

// LunchBreakPolicy is settings for lunch break calculation
type LunchBreakPolicy struct {
	ReductionRate      int64         // minutes per hour
	ReductionThreshold time.Duration // minutes
	ReductionMax       time.Duration // minutes
}

// MarshalJSON implements json Marshaler
func (lb *LunchBreakPolicy) MarshalJSON() ([]byte, error) {
	max := ""
	if lb.ReductionMax != 0 {
		max = fmt.Sprintf("%d min", int(lb.ReductionMax.Minutes()))
	}
	return json.Marshal(struct {
		Rate      string `json:"rate"`
		Threshold string `json:"threshold"`
		Maximum   string `json:"maximum,omitempty"`
	}{
		Rate:      fmt.Sprintf("%d min/h", lb.ReductionRate),
		Threshold: fmt.Sprintf("%.2f h", lb.ReductionThreshold.Hours()),
		Maximum:   max,
	})
}

// SalaryType ...
type SalaryType int

const (
	// SalaryMonthly ...
	SalaryMonthly SalaryType = iota
	// SalaryHourly ...
	SalaryHourly
)

// MarshalJSON implements json Marshaler
func (z *SalaryType) MarshalJSON() ([]byte, error) {
	return json.Marshal(z.String())
}

func (z SalaryType) String() string {
	if z == SalaryHourly {
		return "hourly"
	} else if z == SalaryMonthly {
		return "monthly"
	}
	return ""
}

// SalaryScale struct
type SalaryScale struct {
	ID         int32                  `json:"id"`
	Name       string                 `json:"name"`
	SalaryType SalaryType             `json:"type"`
	Revisions  []*SalaryScaleRevision `json:"revisions"`
}

// SalaryScaleRevision contains salary-scale attributes for the 'Date' point in time
type SalaryScaleRevision struct {
	Date        time.Time        `json:"revision"`
	DefaultRate *amount.DKK      `json:"default_rate,omitempty"`
	Pension     *PensionRates    `json:"pension,omitempty"`
	Rates       SalaryScaleRates `json:"rates,omitempty"`
}

// SalaryScaleRates ...
type SalaryScaleRates []*SalaryScaleRate

// Rate return rate for t time
func (rates *SalaryScaleRates) Rate(t time.Time) (*SalaryScaleRate, error) {
	mins := t.Hour()*60 + t.Minute()

	var wday Weekday
	if t.Weekday() == time.Sunday {
		wday = WeekdaySunday
	} else {
		wday = 1 << uint(t.Weekday())
	}

	var rate *SalaryScaleRate
	for _, r := range *rates {
		if mins >= r.PeriodFrom && mins < r.PeriodTo && (r.WeekDays&wday != 0) {
			rate = r
		}
	}
	if rate == nil {
		return nil, fmt.Errorf("cannot locate rate for time: %v", t)
	}
	return rate, nil
}

// SalaryScaleRate is rate specification
type SalaryScaleRate struct {
	Index      int         `json:"idx"`
	Amount     *amount.DKK `json:"amount"`
	WeekDays   Weekday
	PeriodFrom int
	PeriodTo   int
}

func (r *SalaryScaleRate) String() string {
	hfrom := r.PeriodFrom / 60
	mfrom := r.PeriodFrom - hfrom*60
	hto := r.PeriodTo / 60
	mto := r.PeriodTo - hto*60
	return fmt.Sprintf("%d: %s %02d:%02d-%02d:%02d %s", r.Index, r.Amount, hfrom, mfrom, hto, mto, r.WeekDays)
}

// MarshalJSON implements json Marshaler
func (r *SalaryScaleRate) MarshalJSON() ([]byte, error) {
	return json.Marshal(r.String())
}

// CompanyPolicy struct
type CompanyPolicy struct {
	Date             time.Time                `json:"revision"`
	LunchBreak       *LunchBreakCompanyPolicy `json:"lunchbreaks"`
	HolidayAllowance *HolidayAllowancePolicy  `json:"holiday_allowance"`
	PublicPension    *PensionPolicy           `json:"public_pension"`
	Als              *PublicDutyPolicy        `json:"als"`
	Barsilsgjald     *PublicDutyPolicy        `json:"barsilsgjald"`
	Ameg             *PublicDutyPolicy        `json:"ameg"`
}

// HolidayAllowancePolicy contains rates for holiday allowance
type HolidayAllowancePolicy struct {
	RateFixed  *amount.Percentage `json:"monthly"`
	RateHourly *amount.Percentage `json:"hourly"`
}

// PensionPolicy contains rates for public pension
type PensionPolicy struct {
	Rate   *amount.Percentage `json:"rate"`
	AgeMin int                `json:"age_min"`
	AgeMax int                `json:"age_max"`
}

// PublicDutyPolicy ...
type PublicDutyPolicy struct {
	Rate *amount.Percentage `json:"rate"`
}

// LunchBreakCompanyPolicy contains company-wide settings for lunchbreaks calculation
type LunchBreakCompanyPolicy struct {
	AllowDepartmentJoining bool `json:"join_departments?"`
	ShiftGap               int  `json:"shift_gap"`
}

// EmployeeShift ...
type EmployeeShift struct {
	ID           int32     `json:"id"`
	Checkin      time.Time `json:"checkin"`
	Checkout     time.Time `json:"checkout"`
	DepartmentID int32     `json:"department_id"`
	TaskID       int32     `json:"task_id"`
}

// EmployeeShifts ..
type EmployeeShifts map[int][]*EmployeeShift

// MarshalJSON implements json Marshaler
func (xs EmployeeShifts) MarshalJSON() ([]byte, error) {
	var shifts []*EmployeeShift
	for _, x := range xs {
		for _, e := range x {
			shifts = append(shifts, e)
		}
	}
	return json.Marshal(shifts)
}

type shiftsmapkey struct {
	dep, task int32
	date      time.Time
}

// EmployeeShiftsMap ...
type EmployeeShiftsMap map[shiftsmapkey][]*EmployeeShift

// DayShifts ...
func (m *EmployeeShiftsMap) DayShifts(date time.Time, depID, taskID int32) []*EmployeeShift {
	return (*m)[shiftsmapkey{dep: depID, task: taskID, date: date}]
}

// Push ...
func (m *EmployeeShiftsMap) Push(shift *EmployeeShift) {
	key := shiftsmapkey{date: timeutil.StartOf(shift.Checkin), dep: shift.DepartmentID, task: shift.TaskID}
	(*m)[key] = append((*m)[key], shift)
}

// WagesPeriod ...
type WagesPeriod struct {
	Hourly   *Period
	Monthly  *Period
	CalcDate time.Time
}
