package calc

import (
	"math/big"
	"time"

	"gitlab.com/stjarni/wages"
)

// Segment ...
type Segment struct {
	SegType     SegmentType
	Date        time.Time
	Department  *wages.Department
	Task        *wages.EmployeeTask
	SalaryScale *wages.SalaryScale
	Shift       *wages.Shift
	Bonus       *wages.Bonus
	Duration    *big.Rat
	Rate        *SegmentRate
	Salary      *big.Rat
	LunchBreak  *LunchBreak
}

// SegmentType ...
type SegmentType int

// LunchBreak ...
type LunchBreak struct {
	Duration  *big.Rat
	Reduction *big.Rat
}

// SegmentRate ...
type SegmentRate struct {
	RateIdx int
	Rate    *big.Rat
}
