package calc

import (
	"math/big"
	"time"

	"gitlab.com/stjarni/wages"
	"gitlab.com/stjarni/wages/core/amount"
	"gitlab.com/stjarni/wages/core/timeline"
	"gitlab.com/stjarni/wages/core/timeutil"
)

type monthlyseg struct {
	date        time.Time
	department  *wages.Department
	task        *wages.EmployeeTask
	salaryscale *wages.SalaryScale
	shifts      []*wages.EmployeeShift
	dailyrate   *big.Rat
	splitby     int
	splitratio  *amount.Percentage
	bonuses     []*wages.Bonus
}

// MonthlyWages ...
func MonthlyWages(t *timeline.Timeline, p *wages.Period, shifts wages.EmployeeShiftsMap) ([]*Segment, []*Segment) {
	// defer func() {
	// 	r := recover()
	// 	if r != nil {
	// 		log.Printf("empl: %d", q.Employee.ID)
	// 	}
	// }()

	i := p.To.Add(-24 * time.Hour)
	t.Reset(i)
	t.Rewind(i)

	// calculate number of days in current month and start of the month
	days := timeutil.Date(i.Year(), i.Month()+1, 0).Day()
	monthstart := timeutil.Date(i.Year(), i.Month(), 1)

	var segments [][]*monthlyseg

	i = p.To // back to period.to
	var next time.Time
	for i.After(p.From) {
		// cacluate salary spec for segment
		rev := t.EmployeeRevision()

		if rev == nil {
			break
		}

		// determine segment left bound and arrange data for next iteration
		if !p.From.Before(monthstart) && !p.From.Before(rev.Date) {
			next = p.From
			// last iteration; do nothing
		} else if rev.Date.After(monthstart) {
			next = rev.Date
			// rewind timeline for next iteration
			d := next.Add(-24 * time.Hour)
			t.Rewind(d) // pretty awful; we just need to take next revision in row w/out date calculations
		} else {
			next = monthstart
			// arrange data for next iteration
			days = timeutil.Date(next.Year(), next.Month(), 0).Day()
			monthstart = timeutil.Date(next.Year(), next.Month()-1, 1)
		}

		var daysegs []*monthlyseg
		day := i.Add(-24 * time.Hour)
		for day.After(next) || day.Equal(next) {
			daysegs = nil
			for _, pos := range rev.Positions {
				if pos.SalaryScale.SalaryType == wages.SalaryMonthly {
					rate := new(big.Rat).SetFrac64(rev.MonthySalary.Int64(), int64(days)*100)
					daysegs = append(daysegs, &monthlyseg{
						date:        day,
						department:  pos.Department,
						task:        pos.Task,
						salaryscale: &wages.SalaryScale{ID: pos.SalaryScale.ID, Name: pos.SalaryScale.Name},
						shifts:      shifts.DayShifts(day, pos.Department.ID, pos.Task.ID),
						dailyrate:   rate,
						bonuses:     pos.Bonuses,
						//SplitBy: 1
						splitratio: pos.SplitRatio,
					})
				}
			}
			segments = append(segments, daysegs)
			day = day.Add(-24 * time.Hour)
		}
		i = next
	}

	var wages []*Segment
	var bonuses []*Segment
	for _, daysegments := range segments {
		// TODO: split by duration
		for _, seg := range daysegments {
			var duration int64
			salary := new(big.Rat).Mul(seg.dailyrate, seg.splitratio.Rat())
			for _, sh := range seg.shifts {
				duration += sh.Checkout.Sub(sh.Checkin).Nanoseconds()
			}

			d := new(big.Rat).SetInt64(duration)
			w := Segment{
				Date:        seg.date,
				Department:  seg.department,
				Task:        seg.task,
				SalaryScale: seg.salaryscale,
				Duration:    d,
				Salary:      salary,
			}
			wages = append(wages, &w)

			dayhours := new(big.Rat).SetFrac64(57, 10)
			w.Rate = &SegmentRate{
				Rate: new(big.Rat).Sub(salary, dayhours),
			}

			for _, bseg := range Bonuses(&w, seg.bonuses) {
				bonuses = append(bonuses, bseg)
			}
		}
	}
	return wages, bonuses
}
