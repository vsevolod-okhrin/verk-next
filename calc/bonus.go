package calc

import (
	"math/big"

	"gitlab.com/stjarni/wages"
	"gitlab.com/stjarni/wages/core/amount"
)

// Bonuses ...
func Bonuses(seg *Segment, bonuses []*wages.Bonus) []*Segment {

	var result []*Segment
	for _, bonus := range bonuses {
		bseg := Segment{
			Date:        seg.Date,
			Department:  seg.Department,
			Task:        seg.Task,
			SalaryScale: seg.SalaryScale,
			Shift:       seg.Shift,
		}
		bseg.Bonus = bonus
		bseg.Duration = seg.Duration

		if bonus.AmountType == amount.AmountPercentage {

			r := new(big.Rat).Mul(seg.Rate.Rate, bonus.Percentage.Rat())
			amount := new(big.Rat).Mul(seg.Duration, r)

			bseg.Salary = amount
			result = append(result, &bseg)
		} else if bonus.AmountType == amount.AmountDKK && bonus.Type == wages.SalaryHourly {
			r := new(big.Rat).SetInt64(bonus.Amount.Int64())
			amount := new(big.Rat).Mul(seg.Duration, r)

			bseg.Salary = amount
			result = append(result, &bseg)
		}
	}
	return result
}
