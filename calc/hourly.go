package calc

import (
	"math/big"
	"time"

	"gitlab.com/stjarni/wages"
	"gitlab.com/stjarni/wages/core/amount"
	"gitlab.com/stjarni/wages/core/timeline"
	"gitlab.com/stjarni/wages/core/timeutil"
)

type hourlyseg struct {
	revision    time.Time
	interval    *wages.Period
	shift       *emplshift
	salaryscale *wages.SalaryScale
	defaultrate *amount.DKK
	rates       wages.SalaryScaleRates
	lunchbreak  *wages.LunchBreakPolicy
	bonuses     []*wages.Bonus
}

type emplshift struct {
	id         int32
	checkin    time.Time
	checkout   time.Time
	department *wages.Department
	task       *wages.EmployeeTask
}

// HourlyWages ...
func HourlyWages(t *timeline.Timeline, p *wages.Period, shifts []*wages.EmployeeShift) ([]*Segment, []*Segment, error) {
	t.Reset(p.To)

	groups := group(t, shifts)

	var result []*Segment
	var bonuses []*Segment
	for _, group := range groups {
		if len(group) == 0 {
			continue
		}
		total := group[len(group)-1].interval.To.Sub(group[0].interval.From)
		for _, seg := range group {
			if len(seg.rates) == 0 {
				duration := seg.interval.To.Sub(seg.interval.From)
				rate := new(big.Rat).SetFrac64(seg.defaultrate.Int64(), int64(time.Hour)*100)
				d, salary, lb := HourlySalary(duration, total, seg.defaultrate, seg.lunchbreak)
				hseg := Segment{
					Date:        timeutil.StartOf(seg.interval.From),
					Department:  seg.shift.department,
					Task:        seg.shift.task,
					SalaryScale: seg.salaryscale,
					Shift: &wages.Shift{
						ID:       seg.shift.id,
						Checkin:  seg.shift.checkin,
						Checkout: seg.shift.checkout,
					},
					Duration: d,
					Rate: &SegmentRate{
						RateIdx: 0,
						Rate:    rate,
					},
					Salary:     salary,
					LunchBreak: lb,
				}
				result = append(result, &hseg)

				for _, bseg := range Bonuses(&hseg, seg.bonuses) {
					result = append(result, bseg)
				}
			} else {
				t := seg.interval.From
				for t.Before(seg.interval.To) {
					daystart := timeutil.StartOf(t)
					rate, err := seg.rates.Rate(t)
					if err != nil {
						return nil, nil, err
					}

					to := daystart.Add(time.Minute * time.Duration(rate.PeriodTo))
					if to.After(seg.interval.To) {
						to = seg.interval.To
					}
					duration := to.Sub(t)
					r := new(big.Rat).SetFrac64(rate.Amount.Int64(), int64(time.Hour)*100)
					d, salary, lb := HourlySalary(duration, total, rate.Amount, seg.lunchbreak)
					hseg := Segment{
						Date:        timeutil.StartOf(seg.interval.From),
						Department:  seg.shift.department,
						Task:        seg.shift.task,
						SalaryScale: seg.salaryscale,
						Shift: &wages.Shift{
							ID:       seg.shift.id,
							Checkin:  seg.shift.checkin,
							Checkout: seg.shift.checkout,
						},
						Duration: d,
						Rate: &SegmentRate{
							RateIdx: rate.Index,
							Rate:    r,
						},
						Salary:     salary,
						LunchBreak: lb,
					}
					result = append(result, &hseg)

					for _, bseg := range Bonuses(&hseg, seg.bonuses) {
						bonuses = append(bonuses, bseg)
					}

					t = to
				}
			}
		}
	}
	return result, bonuses, nil
}

func group(t *timeline.Timeline, shifts []*wages.EmployeeShift) [][]*hourlyseg {
	var result [][]*hourlyseg

	if len(shifts) == 0 {
		return result
	}
	if len(shifts) == 1 {
		result = append(result, split(t, shifts[0]))
		return result
	}

	group := split(t, shifts[0])
	for _, shift := range shifts[1:] {
		// do we need to group this shift?
		if len(group) != 0 {
			l := group[len(group)-1]
			policy := t.CompanyPolicy()
			if l.shift.checkin.Sub(shift.Checkout).Minutes() <= float64(policy.LunchBreak.ShiftGap) &&
				(l.shift.department.ID == shift.DepartmentID || policy.LunchBreak.AllowDepartmentJoining) {
				// join please
			} else {
				// finalize group
				result = append(result, group)
				group = nil
			}
		}

		group = append(group, split(t, shift)...)
	}
	return append(result, group)
}

func split(t *timeline.Timeline, shift *wages.EmployeeShift) []*hourlyseg {
	var result []*hourlyseg
	t.Rewind(shift.Checkout)

	// create segment
	revision := t.EmployeeRevision()
	position := revision.Positions.Position(shift.DepartmentID, shift.TaskID)

	if position.SalaryScale.SalaryType == wages.SalaryMonthly {
		return result
	}

	segment := hourlyseg{
		revision: revision.Date,
		interval: &wages.Period{From: shift.Checkin, To: shift.Checkout},
		shift: &emplshift{
			checkin:    shift.Checkin,
			checkout:   shift.Checkout,
			department: position.Department,
			task:       position.Task},
		salaryscale: &wages.SalaryScale{ID: position.SalaryScale.ID, Name: position.SalaryScale.Name},
		defaultrate: position.SalaryScale.DefaultRate,
		rates:       position.SalaryScale.Rates,
		lunchbreak:  position.LunchBreakPolicy,
		bonuses:     position.Bonuses,
	}
	result = append(result, &segment)

	// check if overnight shift
	if !shift.Checkin.Equal(shift.Checkout) {
		t.Rewind(shift.Checkin)
		if !segment.revision.Equal(t.EmployeeRevision().Date) {

			revision := t.EmployeeRevision()
			position := revision.Positions.Position(shift.DepartmentID, shift.TaskID)

			daysegment := hourlyseg{
				revision: revision.Date,
				interval: &wages.Period{
					From: segment.interval.From,
					To:   timeutil.StartOf(segment.interval.To),
				},
				salaryscale: &wages.SalaryScale{ID: position.SalaryScale.ID, Name: position.SalaryScale.Name},
				shift:       segment.shift,
				lunchbreak:  position.LunchBreakPolicy,
				defaultrate: position.SalaryScale.DefaultRate,
				rates:       position.SalaryScale.Rates,
				bonuses:     position.Bonuses,
			}
			segment.interval.From = daysegment.interval.To
			result = append(result, &daysegment)
		}
	}
	return result
}

// HourlySalary ...
func HourlySalary(duration time.Duration, total time.Duration, rate *amount.DKK, lbpolicy *wages.LunchBreakPolicy) (*big.Rat, *big.Rat, *LunchBreak) {
	reduction := new(big.Rat)

	if lbpolicy != nil && total >= lbpolicy.ReductionThreshold {
		// reduction <- segment duration multiplied by reduction rate according to current spec
		reductionrate := new(big.Rat).SetFrac64(lbpolicy.ReductionRate*int64(time.Minute), int64(time.Hour))
		reduction.SetInt64(int64(duration))
		reduction.Mul(reduction, reductionrate)
		if lbpolicy.ReductionMax != 0 {
			// segment reduction cap <- reduction max (according to current spec) normalized to duration fraction
			max := new(big.Rat).SetInt64(int64(lbpolicy.ReductionMax))
			if duration != total {
				max.Mul(max, new(big.Rat).SetFrac64(int64(duration), int64(total))) // weighted reduction max
			}
			if reduction.Cmp(max) == 1 {
				reduction = max
			}
		}
	}

	r := new(big.Rat).SetFrac64(rate.Int64(), int64(time.Hour)*100)
	lb := LunchBreak{
		Duration:  reduction,
		Reduction: new(big.Rat).Mul(reduction, r),
	}

	d := new(big.Rat).SetInt64(int64(duration))
	d.Sub(d, reduction)
	salary := new(big.Rat).Mul(r, d)

	return d, salary, &lb
}
