package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"log"
	"math"
	"net/http"
	"strconv"
	"time"

	_ "github.com/denisenkom/go-mssqldb"
	"github.com/go-chi/chi"
	"gitlab.com/stjarni/wages/core/amount"
	"gitlab.com/stjarni/wages/core/timeutil"
	"gitlab.com/stjarni/wages/query"
	"gitlab.com/stjarni/wages/wages"
)

func main() {
	db, err := sql.Open("sqlserver", "sqlserver://sa:p@ssw0rd@localhost:1401?database=verk")

	if err != nil {
		log.Fatal(err)
	}
	db.SetMaxOpenConns(4)

	r := chi.NewRouter()

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("verk v0.1"))
	})

	r.Get("/employees", func(w http.ResponseWriter, r *http.Request) {
		ctx := context.Background()
		t := time.Now()
		q := query.New(db)
		p := wages.Period{
			From: timeutil.Date(2017, time.January, 1),
			To:   timeutil.Date(2018, time.January, 1),
		}
		empls, err := q.ScanEmployees(ctx, &p)
		if err != nil {
			w.Write([]byte(err.Error()))
			w.WriteHeader(500)
			return
		}

		elapsed := time.Since(t)

		payload := struct {
			Elapsed string            `json:"elapsed"`
			Total   int               `json:"total"`
			Empls   []*wages.Employee `json:"employees"`
		}{
			Elapsed: elapsed.String(),
			Total:   len(empls),
			Empls:   empls,
		}

		b, err := json.Marshal(payload)
		if err != nil {
			w.Write([]byte(err.Error()))
			w.WriteHeader(500)
			return
		}

		w.Header().Add("Content-Type", "application/json")
		w.Write(b)
	})

	r.Get("/employees/{id}", func(w http.ResponseWriter, r *http.Request) {
		ctx := context.Background()
		t := time.Now()
		q := query.New(db)
		p := wages.Period{
			From: timeutil.Date(2017, time.January, 1),
			To:   timeutil.Date(2018, time.January, 1),
		}

		id, err := strconv.Atoi(chi.URLParam(r, "id"))
		if err != nil {
			w.Write([]byte(err.Error()))
			w.WriteHeader(400)
			return
		}

		empls, err := q.Employees(ctx, &p, id)
		if err != nil {
			w.Write([]byte(err.Error()))
			w.WriteHeader(500)
			return
		}

		elapsed := time.Since(t)

		payload := struct {
			Elapsed string            `json:"elapsed"`
			Total   int               `json:"total"`
			Empls   []*wages.Employee `json:"employees"`
		}{
			Elapsed: elapsed.String(),
			Total:   len(empls),
			Empls:   empls,
		}

		b, err := json.Marshal(payload)
		if err != nil {
			w.Write([]byte(err.Error()))
			w.WriteHeader(500)
			return
		}

		w.Header().Add("Content-Type", "application/json")
		w.Write(b)
	})

	r.Get("/company", func(w http.ResponseWriter, r *http.Request) {
		ctx := context.Background()
		t := time.Now()
		q := query.New(db)
		p := wages.Period{
			From: timeutil.Date(2017, time.January, 1),
			To:   timeutil.Date(2018, time.January, 1),
		}
		policies, err := q.CompanyPolicies(ctx, &p)
		if err != nil {
			w.Write([]byte(err.Error()))
			w.WriteHeader(500)
			return
		}

		elapsed := time.Since(t)

		payload := struct {
			Elapsed  string                 `json:"elapsed"`
			Total    int                    `json:"total"`
			Policies []*wages.CompanyPolicy `json:"policies"`
		}{
			Elapsed:  elapsed.String(),
			Total:    len(policies),
			Policies: policies,
		}

		b, err := json.Marshal(payload)
		if err != nil {
			w.Write([]byte(err.Error()))
			w.WriteHeader(500)
			return
		}

		w.Header().Add("Content-Type", "application/json")
		w.Write(b)
	})

	r.Get("/shifts", func(w http.ResponseWriter, r *http.Request) {
		ctx := context.Background()
		t := time.Now()
		q := query.New(db)
		p := wages.Period{
			From: timeutil.Date(2017, time.January, 1),
			To:   timeutil.Date(2018, time.January, 1),
		}
		emplshifts, err := q.EmployeeShifts(ctx, &p)
		if err != nil {
			w.Write([]byte(err.Error()))
			w.WriteHeader(500)
			return
		}

		elapsed := time.Since(t)

		var shifts []*wages.EmployeeShift
		for _, x := range emplshifts {
			for _, e := range x {
				shifts = append(shifts, e)
			}
		}

		payload := struct {
			Elapsed string                 `json:"elapsed"`
			Total   int                    `json:"total"`
			Shifts  []*wages.EmployeeShift `json:"shifts"`
		}{
			Elapsed: elapsed.String(),
			Total:   len(shifts),
			Shifts:  shifts,
		}

		b, err := json.Marshal(payload)
		if err != nil {
			w.Write([]byte(err.Error()))
			w.WriteHeader(500)
			return
		}

		w.Header().Add("Content-Type", "application/json")
		w.Write(b)
	})

	r.Get("/wages", func(w http.ResponseWriter, r *http.Request) {
		ctx := context.Background()
		t := time.Now()
		q := query.New(db)
		wages := wages.NewService(q)
		p := wages.Period{
			From: timeutil.Date(2017, time.January, 1),
			To:   timeutil.Date(2018, time.January, 1),
		}

		wres, err := wages.Wages(ctx, &p)
		if err != nil {
			w.Write([]byte(err.Error()))
			w.WriteHeader(500)
			return
		}

		elapsed := time.Since(t)

		payload := struct {
			Elapsed string               `json:"elapsed"`
			Total   int                  `json:"total"`
			Wages   []*wages.WagesResult `json:"wages"`
		}{
			Elapsed: elapsed.String(),
			Total:   len(wres),
			Wages:   nil,
		}

		b, err := json.Marshal(payload)
		if err != nil {
			w.Write([]byte(err.Error()))
			w.WriteHeader(500)
			return
		}

		w.Header().Add("Content-Type", "application/json")
		w.Write(b)
	})

	r.Get("/wages/{id}", func(w http.ResponseWriter, r *http.Request) {
		ctx := context.Background()
		t := time.Now()
		q := query.New(db)
		wages := wages.NewService(q)
		p := wages.Period{
			From: timeutil.Date(2017, time.January, 1),
			To:   timeutil.Date(2018, time.January, 1),
		}

		id, err := strconv.Atoi(chi.URLParam(r, "id"))
		if err != nil {
			w.Write([]byte(err.Error()))
			w.WriteHeader(400)
			return
		}

		wres, err := wages.Wages(ctx, &p, id)
		if err != nil {
			w.Write([]byte(err.Error()))
			w.WriteHeader(500)
			return
		}

		elapsed := time.Since(t)

		type wagesrate struct {
			Duration  string
			Reduction string
			RateIdx   int
			Rate      *amount.DKK
		}

		type wagesfee struct {
			HolidayAllowance             *amount.DKK
			Pension                      *amount.DKK
			UnionMembershipFee           *amount.DKK
			UnionInsuranceFee            *amount.DKK
			LocalDepartmentMembershipFee *amount.DKK
			Als                          *amount.DKK
			Ameg                         *amount.DKK
			Barsilsgjald                 *amount.DKK
		}

		type wage struct {
			Date        time.Time
			Department  *wages.Department
			Task        *wages.EmployeeTask
			SalaryScale *wages.SalaryScale
			Rate        *wagesrate
			Shift       *wages.EmployeeShift
			Salary      *amount.DKK
			Fees        *wagesfee
		}

		var ws []*wage
		for _, w := range wres {
			var wr *wagesrate
			if w.Wages.Rate != nil {
				d, _ := w.Wages.Duration.Float64()
				r, _ := w.Wages.LunchBreak.Duration.Float64()
				wr = &wagesrate{
					Duration:  time.Duration(int64(math.Round(d))).String(),
					Reduction: time.Duration(int64(math.Round(r))).String(),
					RateIdx:   w.Wages.Rate.RateIdx,
					Rate:      w.Wages.Rate.Rate,
				}
			}

			ws = append(ws, &wage{
				Date:        w.Wages.Date,
				Department:  w.Wages.Department,
				Task:        w.Wages.Task,
				SalaryScale: w.Wages.SalaryScale,
				Shift:       w.Wages.Shift,
				Rate:        wr,
				Salary:      new(amount.DKK).SetRat(w.Wages.Salary),
				Fees:        &wagesfee{},
			})
		}

		payload := struct {
			Elapsed string  `json:"elapsed"`
			Total   int     `json:"total"`
			Wages   []*wage `json:"wages"`
		}{
			Elapsed: elapsed.String(),
			Total:   len(ws),
			Wages:   ws,
		}

		b, err := json.Marshal(payload)
		if err != nil {
			w.Write([]byte(err.Error()))
			w.WriteHeader(500)
			return
		}

		w.Header().Add("Content-Type", "application/json")
		w.Write(b)
	})

	server := http.Server{Addr: ":8080", Handler: r}

	log.Println("listening on :8080")
	err = server.ListenAndServe()
	log.Println(err)
}
