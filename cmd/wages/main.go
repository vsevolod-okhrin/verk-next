package main

import (
	"database/sql"
	"fmt"
	"log"
	"net"
	"os"
	"strings"

	"gitlab.com/stjarni/wages/service"
	"google.golang.org/grpc"
)

func main() {
	apps := make(map[string]*sql.DB)

	for _, e := range os.Environ() {
		if strings.HasPrefix(e, "WAGES_DB_") {
			pair := strings.Split(e, "=")

			var app string
			app = strings.TrimPrefix(pair[0], "WAGES_DB_")
			app = strings.ToLower(app)
			connstring := pair[1]

			db, err := sql.Open("sqlserver", connstring)
			if err != nil {
				log.Fatal(err)
			}

			log.Printf("register app '%s' connection", app)
			db.SetMaxOpenConns(4)
			apps[app] = db
		}
	}

	s := service.NewServer(&apps)
	grpcServer := grpc.NewServer()
	service.RegisterWagesServiceServer(grpcServer, s)

	addr, ok := os.LookupEnv("WAGES_ADDR")
	if !ok {
		addr = "localhost"
	}
	port, ok := os.LookupEnv("WAGES_PORT")
	if !ok {
		port = "50052"
	}

	lis, err := net.Listen("tcp", fmt.Sprintf("%s:%s", addr, port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	fmt.Println("listening on localhost:50052")
	grpcServer.Serve(lis)
}
