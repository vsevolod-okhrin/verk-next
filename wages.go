package wages

import (
	"context"
	"encoding/json"
	"time"

	"gitlab.com/stjarni/wages/core/amount"
)

// Weekday flags
type Weekday uint32

// Weekday flags
const (
	WeekdayMonday Weekday = 1 << iota
	WeekdayTuesday
	WeekdayWednesday
	WeekdayThursday
	WeekdayFriday
	WeekdaySaturday
	WeekdaySunday
)

func (w Weekday) String() string {
	switch w {
	case WeekdayMonday:
		return "monday"
	case WeekdayTuesday:
		return "tuesday"
	case WeekdayWednesday:
		return "wednesday"
	case WeekdayThursday:
		return "thursday"
	case WeekdayFriday:
		return "friday"
	case WeekdaySaturday:
		return "saturday"
	case WeekdaySunday:
		return "sunday"
	case WeekdayMonday | WeekdayTuesday | WeekdayWednesday | WeekdayThursday | WeekdayFriday:
		return "weekdays"
	}
	return "unknown"
}

// Gender
const (
	GenderMale Gender = iota
	GenderFemale
)

// Gender option
type Gender int

func (g Gender) String() string {
	if g == GenderMale {
		return "male"
	} else if g == GenderFemale {
		return "female"
	}
	return ""
}

// MarshalJSON is json Marshaler impl
func (g Gender) MarshalJSON() ([]byte, error) {
	return json.Marshal(g.String())
}

// Period is a date interval; right bound is exclusive
type Period struct {
	From time.Time
	To   time.Time
}

// Employee struct
type Employee struct {
	ID        int                 `json:"id"`
	Name      string              `json:"name"`
	Ptal      string              `json:"ptal,omitempty"`
	Gender    Gender              `json:"gender"`
	BirthDate time.Time           `json:"birthdate,omitempty"`
	Email     string              `json:"email"`
	Address   string              `json:"address,omitempty"`
	Phone     string              `json:"phone,omitempty"`
	Revisions []*EmployeeRevision `json:"revisions"`
}

// Union struct
type Union struct {
	ID    int32  `json:"id"`
	Name  string `json:"name"`
	Email string `json:"email,omitempty"`
	Phone string `json:"phone,omitempty"`
}

// UnionRevision contains union attributes for the 'Date' point in time
type UnionRevision struct {
	Date                    time.Time
	MembershipFee           *FeeRates
	InsuranceFee            *FeeRates
	Pension                 *PensionRates
	MembershipFeeAccount    *BankAccount
	InsuranceFeeAccount     *BankAccount
	PensionAccount          *BankAccount
	HolidayAllowanceAccount *BankAccount
}

// LocalDepartment struct
type LocalDepartment struct {
	ID   int32  `json:"id"`
	Name string `json:"name"`
}

// WagesService interface
type WagesService interface {
	Wages(ctx context.Context, p *Period, emplids ...int) ([]*WagesResult, error)
}

// QueryResult contains 'query stage' result for wages
type QueryResult struct {
	Employee        *Employee
	EmployeeShifts  []*EmployeeShift
	CompanyPolicies []*CompanyPolicy
}

// WagesResult struct
type WagesResult struct {
	Employee  *Employee
	Wages     *WagesSegment
	WagesFees *WagesFees
}

// WagesSegment ...
type WagesSegment struct {
	SegType     SegmentType
	Date        time.Time
	Department  *Department
	Task        *EmployeeTask
	SalaryScale *SalaryScale
	Shift       *Shift
	Bonus       *Bonus
	Duration    time.Duration
	Rate        *WagesSegmentRate
	Salary      *amount.DKK
	LunchBreak  *LunchBreak
}

// SegmentType enum
type SegmentType int

// SegmentType enum values
const (
	SegmentUnknown SegmentType = iota
	SegmentHourly
	SegmentMonthly
	SegmentBonus
	SegmentOvertime
)

// Shift ...
type Shift struct {
	ID       int32
	Checkin  time.Time
	Checkout time.Time
}

// LunchBreak ...
type LunchBreak struct {
	Duration  time.Duration
	Reduction *amount.DKK
}

// WagesSegmentRate ...
type WagesSegmentRate struct {
	RateIdx int
	Rate    *amount.DKK
}

// WagesFees ...
type WagesFees struct {
	HolidayAllowance             *amount.DKK
	Pension                      *amount.DKK
	UnionMembershipFee           *amount.DKK
	UnionInsuranceFee            *amount.DKK
	LocalDepartmentMembershipFee *amount.DKK
	Als                          *amount.DKK
	Ameg                         *amount.DKK
	Barsilsgjald                 *amount.DKK
}
