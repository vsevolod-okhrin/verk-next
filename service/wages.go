package service

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"math/big"
	"strings"
	"time"

	"github.com/golang/protobuf/ptypes"
	"gitlab.com/stjarni/wages"
	"gitlab.com/stjarni/wages/calc"
	"gitlab.com/stjarni/wages/core/amount"
	"gitlab.com/stjarni/wages/core/timeline"
	"gitlab.com/stjarni/wages/query"
)

type service struct {
	apps map[string]*sql.DB
}

// NewServer creates new wages service service
func NewServer(apps *map[string]*sql.DB) WagesServiceServer {
	return &service{
		apps: *apps,
	}
}

func (svc *service) EmployeeWages(ctx context.Context, in *EmployeeWagesRequest) (*EmployeeWagesResponse, error) {
	response := new(EmployeeWagesResponse)
	return response, nil
}

func (svc *service) Wages(ctx context.Context, in *WagesRequest) (*WagesResponse, error) {

	app := "verk"
	db, ok := svc.apps[app]
	if !ok {
		return nil, fmt.Errorf("missing connection string for app: %s. please specify with env var WAGES_DB_%s", app, strings.ToUpper(app))
	}
	q := query.New(db)

	from, err := ptypes.Timestamp(in.Interval.From)
	if err != nil {
		return nil, err
	}
	to, err := ptypes.Timestamp(in.Interval.To)
	if err != nil {
		return nil, err
	}
	p := wages.Period{From: from, To: to}

	wperiods, err := q.Periods(ctx, &p)
	if err != nil {
		log.Print(err)
		return nil, err
	}

	policies, err := q.CompanyPolicies(ctx, &p)
	if err != nil {
		return nil, err
	}
	empls, err := q.Employees(ctx, &p, in.EmployeeId...)
	if err != nil {
		return nil, err
	}
	shifts, err := q.EmployeeShifts(ctx, &p, empls...)
	if err != nil {
		return nil, err
	}

	var result []*WagesResult
	for _, empl := range empls {

		// log.Printf("calc wages for empl: %d, %s\n", empl.ID, empl.Name)
		for _, wperiod := range wperiods {
			emplres := WagesResult{
				Employee: &Employee{
					Id:   int32(empl.ID),
					Name: empl.Name,
				},
			}

			ws, err := emplwages(&wages.QueryResult{
				Employee:        empl,
				EmployeeShifts:  shifts[empl.ID],
				CompanyPolicies: policies,
			}, wperiod)
			if err != nil {
				return nil, err
			}

			for _, seg := range ws {
				date, err := ptypes.TimestampProto(seg.Date)
				if err != nil {
					return nil, err
				}
				w := EmployeeWagesResult{
					Type:        EmployeeWagesResult_WagesType(seg.SegType),
					Date:        date,
					Departments: &Department{Id: seg.Department.ID, Name: seg.Department.Name},
					Task:        &EmployeeTask{Id: seg.Task.ID, Name: seg.Task.Name},

					Salary: seg.Salary.Int64(),
				}
				if seg.Shift != nil {
					checkin, err := ptypes.TimestampProto(seg.Shift.Checkin)
					if err != nil {
						return nil, err
					}
					checkout, err := ptypes.TimestampProto(seg.Shift.Checkout)
					if err != nil {
						return nil, err
					}
					w.Shift = &EmployeeShift{
						Id:       seg.Shift.ID,
						Checkin:  checkin,
						Checkout: checkout,
					}
				}
				emplres.Wages = append(emplres.Wages, &w)
			}

			result = append(result, &emplres)
		}
	}

	response := new(WagesResponse)
	response.Empls = result
	return response, nil
}

func emplwages(q *wages.QueryResult, p *wages.WagesPeriod) ([]*wages.WagesSegment, error) {
	var result []*wages.WagesSegment

	t := timeline.New(q.CompanyPolicies, q.Employee.Revisions)

	var shifts []*wages.EmployeeShift
	shiftsmap := make(wages.EmployeeShiftsMap)
	for _, sh := range q.EmployeeShifts {
		if (sh.Checkin.After(p.Hourly.From) || sh.Checkin.Equal(p.Hourly.From)) && sh.Checkout.Before(p.Hourly.To) {
			shifts = append(shifts, sh)
		}

		if sh.Checkin.Before(p.Monthly.To) && !sh.Checkin.Before(p.Monthly.From) {
			shiftsmap.Push(sh)
		}
	}

	hwages, bonuses, err := calc.HourlyWages(t, p.Hourly, shifts)
	if err != nil {
		return nil, err
	}
	// calcm := make(map[int32][]*calc.Segment)
	// wagesm := make(map[int32][]*wages.WagesSegment)
	for _, seg := range hwages {
		nanosecs, _ := seg.Duration.Float64()
		wseg := wages.WagesSegment{
			SegType:    wages.SegmentHourly,
			Date:       seg.Date,
			Department: seg.Department,
			Task:       seg.Task,
			Shift:      seg.Shift,
			Duration:   time.Duration(nanosecs) / time.Second,
			Salary:     new(amount.DKK).SetRat(seg.Salary),
			Rate: &wages.WagesSegmentRate{
				RateIdx: seg.Rate.RateIdx,
				Rate:    new(amount.DKK).SetRat(seg.Rate.Rate),
			},
		}
		if seg.LunchBreak != nil {
			duration, _ := seg.LunchBreak.Duration.Float64()
			wseg.LunchBreak = &wages.LunchBreak{
				Duration:  time.Duration(duration) / time.Second,
				Reduction: new(amount.DKK).SetRat(seg.LunchBreak.Reduction),
			}
		}
		// calcm[seg.Shift.ID] = append(calcm[seg.Shift.ID], seg)
		// wagesm[seg.Shift.ID] = append(wagesm[seg.Shift.ID], wseg)
		result = append(result, &wseg)
	}
	// TODO: rounding

	for _, seg := range bonuses {
		wseg := wages.WagesSegment{
			SegType:     wages.SegmentBonus,
			Date:        seg.Date,
			Department:  seg.Department,
			Task:        seg.Task,
			SalaryScale: seg.SalaryScale,
			Bonus:       seg.Bonus,
			Salary:      new(amount.DKK).SetRat(seg.Salary),
		}
		result = append(result, &wseg)
	}

	mwages, bonuses := calc.MonthlyWages(t, p.Monthly, shiftsmap)

	msalaryrat := new(big.Rat)
	msalarydkk := new(amount.DKK)
	for _, m := range mwages {
		nanosecods, _ := m.Duration.Float64()
		duration := time.Duration(nanosecods) / time.Second
		salary := new(amount.DKK).SetRat(m.Salary)

		msalaryrat.Add(msalaryrat, m.Salary)
		msalarydkk.Add(msalarydkk, salary)

		w := wages.WagesSegment{
			SegType:     wages.SegmentMonthly,
			Date:        m.Date,
			Department:  m.Department,
			Task:        m.Task,
			SalaryScale: m.SalaryScale,
			Duration:    duration,
			Salary:      salary,
		}

		result = append(result, &w)
	}

	remainder := new(amount.DKK).SetRat(msalaryrat)
	remainder.Sub(remainder, msalarydkk)
	result[len(result)-1].Salary.Inc(remainder)

	for _, seg := range bonuses {
		wseg := wages.WagesSegment{
			SegType:     wages.SegmentBonus,
			Date:        seg.Date,
			Department:  seg.Department,
			Task:        seg.Task,
			SalaryScale: seg.SalaryScale,
			Bonus:       seg.Bonus,
			Salary:      new(amount.DKK).SetRat(seg.Salary),
		}
		result = append(result, &wseg)
	}

	// apply fees

	return result, nil
}
