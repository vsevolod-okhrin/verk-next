// Code generated by protoc-gen-go. DO NOT EDIT.
// source: wages.proto

package service

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"
import timestamp "github.com/golang/protobuf/ptypes/timestamp"

import (
	context "golang.org/x/net/context"
	grpc "google.golang.org/grpc"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type EmployeeWagesResult_WagesType int32

const (
	EmployeeWagesResult_UNKNOWN  EmployeeWagesResult_WagesType = 0
	EmployeeWagesResult_HOURLY   EmployeeWagesResult_WagesType = 1
	EmployeeWagesResult_MONHTLY  EmployeeWagesResult_WagesType = 2
	EmployeeWagesResult_BONUS    EmployeeWagesResult_WagesType = 3
	EmployeeWagesResult_OVERTIME EmployeeWagesResult_WagesType = 4
)

var EmployeeWagesResult_WagesType_name = map[int32]string{
	0: "UNKNOWN",
	1: "HOURLY",
	2: "MONHTLY",
	3: "BONUS",
	4: "OVERTIME",
}
var EmployeeWagesResult_WagesType_value = map[string]int32{
	"UNKNOWN":  0,
	"HOURLY":   1,
	"MONHTLY":  2,
	"BONUS":    3,
	"OVERTIME": 4,
}

func (x EmployeeWagesResult_WagesType) String() string {
	return proto.EnumName(EmployeeWagesResult_WagesType_name, int32(x))
}
func (EmployeeWagesResult_WagesType) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_wages_096eba28521780f5, []int{6, 0}
}

type Bonus_BonusType int32

const (
	Bonus_UNKNOWN Bonus_BonusType = 0
	Bonus_BONUS   Bonus_BonusType = 1
	Bonus_EXPENSE Bonus_BonusType = 2
)

var Bonus_BonusType_name = map[int32]string{
	0: "UNKNOWN",
	1: "BONUS",
	2: "EXPENSE",
}
var Bonus_BonusType_value = map[string]int32{
	"UNKNOWN": 0,
	"BONUS":   1,
	"EXPENSE": 2,
}

func (x Bonus_BonusType) String() string {
	return proto.EnumName(Bonus_BonusType_name, int32(x))
}
func (Bonus_BonusType) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_wages_096eba28521780f5, []int{11, 0}
}

type EmployeeWagesRequest struct {
	Interval             *Interval `protobuf:"bytes,1,opt,name=interval,proto3" json:"interval,omitempty"`
	EmployeeId           int32     `protobuf:"varint,2,opt,name=employee_id,json=employeeId,proto3" json:"employee_id,omitempty"`
	XXX_NoUnkeyedLiteral struct{}  `json:"-"`
	XXX_unrecognized     []byte    `json:"-"`
	XXX_sizecache        int32     `json:"-"`
}

func (m *EmployeeWagesRequest) Reset()         { *m = EmployeeWagesRequest{} }
func (m *EmployeeWagesRequest) String() string { return proto.CompactTextString(m) }
func (*EmployeeWagesRequest) ProtoMessage()    {}
func (*EmployeeWagesRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_wages_096eba28521780f5, []int{0}
}
func (m *EmployeeWagesRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_EmployeeWagesRequest.Unmarshal(m, b)
}
func (m *EmployeeWagesRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_EmployeeWagesRequest.Marshal(b, m, deterministic)
}
func (dst *EmployeeWagesRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_EmployeeWagesRequest.Merge(dst, src)
}
func (m *EmployeeWagesRequest) XXX_Size() int {
	return xxx_messageInfo_EmployeeWagesRequest.Size(m)
}
func (m *EmployeeWagesRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_EmployeeWagesRequest.DiscardUnknown(m)
}

var xxx_messageInfo_EmployeeWagesRequest proto.InternalMessageInfo

func (m *EmployeeWagesRequest) GetInterval() *Interval {
	if m != nil {
		return m.Interval
	}
	return nil
}

func (m *EmployeeWagesRequest) GetEmployeeId() int32 {
	if m != nil {
		return m.EmployeeId
	}
	return 0
}

type EmployeeWagesResponse struct {
	Wages                []*EmployeeWagesResult `protobuf:"bytes,1,rep,name=wages,proto3" json:"wages,omitempty"`
	XXX_NoUnkeyedLiteral struct{}               `json:"-"`
	XXX_unrecognized     []byte                 `json:"-"`
	XXX_sizecache        int32                  `json:"-"`
}

func (m *EmployeeWagesResponse) Reset()         { *m = EmployeeWagesResponse{} }
func (m *EmployeeWagesResponse) String() string { return proto.CompactTextString(m) }
func (*EmployeeWagesResponse) ProtoMessage()    {}
func (*EmployeeWagesResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_wages_096eba28521780f5, []int{1}
}
func (m *EmployeeWagesResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_EmployeeWagesResponse.Unmarshal(m, b)
}
func (m *EmployeeWagesResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_EmployeeWagesResponse.Marshal(b, m, deterministic)
}
func (dst *EmployeeWagesResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_EmployeeWagesResponse.Merge(dst, src)
}
func (m *EmployeeWagesResponse) XXX_Size() int {
	return xxx_messageInfo_EmployeeWagesResponse.Size(m)
}
func (m *EmployeeWagesResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_EmployeeWagesResponse.DiscardUnknown(m)
}

var xxx_messageInfo_EmployeeWagesResponse proto.InternalMessageInfo

func (m *EmployeeWagesResponse) GetWages() []*EmployeeWagesResult {
	if m != nil {
		return m.Wages
	}
	return nil
}

type WagesRequest struct {
	Interval             *Interval `protobuf:"bytes,1,opt,name=interval,proto3" json:"interval,omitempty"`
	EmployeeId           []int32   `protobuf:"varint,2,rep,packed,name=employee_id,json=employeeId,proto3" json:"employee_id,omitempty"`
	DepartmentId         int32     `protobuf:"varint,3,opt,name=department_id,json=departmentId,proto3" json:"department_id,omitempty"`
	TaskId               int32     `protobuf:"varint,4,opt,name=task_id,json=taskId,proto3" json:"task_id,omitempty"`
	XXX_NoUnkeyedLiteral struct{}  `json:"-"`
	XXX_unrecognized     []byte    `json:"-"`
	XXX_sizecache        int32     `json:"-"`
}

func (m *WagesRequest) Reset()         { *m = WagesRequest{} }
func (m *WagesRequest) String() string { return proto.CompactTextString(m) }
func (*WagesRequest) ProtoMessage()    {}
func (*WagesRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_wages_096eba28521780f5, []int{2}
}
func (m *WagesRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_WagesRequest.Unmarshal(m, b)
}
func (m *WagesRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_WagesRequest.Marshal(b, m, deterministic)
}
func (dst *WagesRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_WagesRequest.Merge(dst, src)
}
func (m *WagesRequest) XXX_Size() int {
	return xxx_messageInfo_WagesRequest.Size(m)
}
func (m *WagesRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_WagesRequest.DiscardUnknown(m)
}

var xxx_messageInfo_WagesRequest proto.InternalMessageInfo

func (m *WagesRequest) GetInterval() *Interval {
	if m != nil {
		return m.Interval
	}
	return nil
}

func (m *WagesRequest) GetEmployeeId() []int32 {
	if m != nil {
		return m.EmployeeId
	}
	return nil
}

func (m *WagesRequest) GetDepartmentId() int32 {
	if m != nil {
		return m.DepartmentId
	}
	return 0
}

func (m *WagesRequest) GetTaskId() int32 {
	if m != nil {
		return m.TaskId
	}
	return 0
}

type WagesResponse struct {
	Empls                []*WagesResult `protobuf:"bytes,1,rep,name=empls,proto3" json:"empls,omitempty"`
	XXX_NoUnkeyedLiteral struct{}       `json:"-"`
	XXX_unrecognized     []byte         `json:"-"`
	XXX_sizecache        int32          `json:"-"`
}

func (m *WagesResponse) Reset()         { *m = WagesResponse{} }
func (m *WagesResponse) String() string { return proto.CompactTextString(m) }
func (*WagesResponse) ProtoMessage()    {}
func (*WagesResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_wages_096eba28521780f5, []int{3}
}
func (m *WagesResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_WagesResponse.Unmarshal(m, b)
}
func (m *WagesResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_WagesResponse.Marshal(b, m, deterministic)
}
func (dst *WagesResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_WagesResponse.Merge(dst, src)
}
func (m *WagesResponse) XXX_Size() int {
	return xxx_messageInfo_WagesResponse.Size(m)
}
func (m *WagesResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_WagesResponse.DiscardUnknown(m)
}

var xxx_messageInfo_WagesResponse proto.InternalMessageInfo

func (m *WagesResponse) GetEmpls() []*WagesResult {
	if m != nil {
		return m.Empls
	}
	return nil
}

type WagesResult struct {
	Employee             *Employee              `protobuf:"bytes,1,opt,name=employee,proto3" json:"employee,omitempty"`
	Wages                []*EmployeeWagesResult `protobuf:"bytes,2,rep,name=wages,proto3" json:"wages,omitempty"`
	XXX_NoUnkeyedLiteral struct{}               `json:"-"`
	XXX_unrecognized     []byte                 `json:"-"`
	XXX_sizecache        int32                  `json:"-"`
}

func (m *WagesResult) Reset()         { *m = WagesResult{} }
func (m *WagesResult) String() string { return proto.CompactTextString(m) }
func (*WagesResult) ProtoMessage()    {}
func (*WagesResult) Descriptor() ([]byte, []int) {
	return fileDescriptor_wages_096eba28521780f5, []int{4}
}
func (m *WagesResult) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_WagesResult.Unmarshal(m, b)
}
func (m *WagesResult) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_WagesResult.Marshal(b, m, deterministic)
}
func (dst *WagesResult) XXX_Merge(src proto.Message) {
	xxx_messageInfo_WagesResult.Merge(dst, src)
}
func (m *WagesResult) XXX_Size() int {
	return xxx_messageInfo_WagesResult.Size(m)
}
func (m *WagesResult) XXX_DiscardUnknown() {
	xxx_messageInfo_WagesResult.DiscardUnknown(m)
}

var xxx_messageInfo_WagesResult proto.InternalMessageInfo

func (m *WagesResult) GetEmployee() *Employee {
	if m != nil {
		return m.Employee
	}
	return nil
}

func (m *WagesResult) GetWages() []*EmployeeWagesResult {
	if m != nil {
		return m.Wages
	}
	return nil
}

type Interval struct {
	From                 *timestamp.Timestamp `protobuf:"bytes,1,opt,name=from,proto3" json:"from,omitempty"`
	To                   *timestamp.Timestamp `protobuf:"bytes,2,opt,name=to,proto3" json:"to,omitempty"`
	XXX_NoUnkeyedLiteral struct{}             `json:"-"`
	XXX_unrecognized     []byte               `json:"-"`
	XXX_sizecache        int32                `json:"-"`
}

func (m *Interval) Reset()         { *m = Interval{} }
func (m *Interval) String() string { return proto.CompactTextString(m) }
func (*Interval) ProtoMessage()    {}
func (*Interval) Descriptor() ([]byte, []int) {
	return fileDescriptor_wages_096eba28521780f5, []int{5}
}
func (m *Interval) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Interval.Unmarshal(m, b)
}
func (m *Interval) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Interval.Marshal(b, m, deterministic)
}
func (dst *Interval) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Interval.Merge(dst, src)
}
func (m *Interval) XXX_Size() int {
	return xxx_messageInfo_Interval.Size(m)
}
func (m *Interval) XXX_DiscardUnknown() {
	xxx_messageInfo_Interval.DiscardUnknown(m)
}

var xxx_messageInfo_Interval proto.InternalMessageInfo

func (m *Interval) GetFrom() *timestamp.Timestamp {
	if m != nil {
		return m.From
	}
	return nil
}

func (m *Interval) GetTo() *timestamp.Timestamp {
	if m != nil {
		return m.To
	}
	return nil
}

type EmployeeWagesResult struct {
	Type                 EmployeeWagesResult_WagesType   `protobuf:"varint,1,opt,name=type,proto3,enum=service.EmployeeWagesResult_WagesType" json:"type,omitempty"`
	Departments          *Department                     `protobuf:"bytes,2,opt,name=departments,proto3" json:"departments,omitempty"`
	Task                 *EmployeeTask                   `protobuf:"bytes,3,opt,name=task,proto3" json:"task,omitempty"`
	Date                 *timestamp.Timestamp            `protobuf:"bytes,4,opt,name=date,proto3" json:"date,omitempty"`
	Shift                *EmployeeShift                  `protobuf:"bytes,5,opt,name=shift,proto3" json:"shift,omitempty"`
	Duration             int64                           `protobuf:"varint,6,opt,name=duration,proto3" json:"duration,omitempty"`
	Salary               int64                           `protobuf:"varint,7,opt,name=salary,proto3" json:"salary,omitempty"`
	Lunchbreak           *EmployeeWagesResult_LunchBreak `protobuf:"bytes,8,opt,name=lunchbreak,proto3" json:"lunchbreak,omitempty"`
	Bonus                *Bonus                          `protobuf:"bytes,9,opt,name=bonus,proto3" json:"bonus,omitempty"`
	Fees                 *EmployeeWagesResult_WagesFees  `protobuf:"bytes,10,opt,name=fees,proto3" json:"fees,omitempty"`
	TotalSalary          int64                           `protobuf:"varint,11,opt,name=TotalSalary,proto3" json:"TotalSalary,omitempty"`
	SalaryToPay          int64                           `protobuf:"varint,12,opt,name=SalaryToPay,proto3" json:"SalaryToPay,omitempty"`
	SalaryCost           int64                           `protobuf:"varint,13,opt,name=SalaryCost,proto3" json:"SalaryCost,omitempty"`
	XXX_NoUnkeyedLiteral struct{}                        `json:"-"`
	XXX_unrecognized     []byte                          `json:"-"`
	XXX_sizecache        int32                           `json:"-"`
}

func (m *EmployeeWagesResult) Reset()         { *m = EmployeeWagesResult{} }
func (m *EmployeeWagesResult) String() string { return proto.CompactTextString(m) }
func (*EmployeeWagesResult) ProtoMessage()    {}
func (*EmployeeWagesResult) Descriptor() ([]byte, []int) {
	return fileDescriptor_wages_096eba28521780f5, []int{6}
}
func (m *EmployeeWagesResult) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_EmployeeWagesResult.Unmarshal(m, b)
}
func (m *EmployeeWagesResult) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_EmployeeWagesResult.Marshal(b, m, deterministic)
}
func (dst *EmployeeWagesResult) XXX_Merge(src proto.Message) {
	xxx_messageInfo_EmployeeWagesResult.Merge(dst, src)
}
func (m *EmployeeWagesResult) XXX_Size() int {
	return xxx_messageInfo_EmployeeWagesResult.Size(m)
}
func (m *EmployeeWagesResult) XXX_DiscardUnknown() {
	xxx_messageInfo_EmployeeWagesResult.DiscardUnknown(m)
}

var xxx_messageInfo_EmployeeWagesResult proto.InternalMessageInfo

func (m *EmployeeWagesResult) GetType() EmployeeWagesResult_WagesType {
	if m != nil {
		return m.Type
	}
	return EmployeeWagesResult_UNKNOWN
}

func (m *EmployeeWagesResult) GetDepartments() *Department {
	if m != nil {
		return m.Departments
	}
	return nil
}

func (m *EmployeeWagesResult) GetTask() *EmployeeTask {
	if m != nil {
		return m.Task
	}
	return nil
}

func (m *EmployeeWagesResult) GetDate() *timestamp.Timestamp {
	if m != nil {
		return m.Date
	}
	return nil
}

func (m *EmployeeWagesResult) GetShift() *EmployeeShift {
	if m != nil {
		return m.Shift
	}
	return nil
}

func (m *EmployeeWagesResult) GetDuration() int64 {
	if m != nil {
		return m.Duration
	}
	return 0
}

func (m *EmployeeWagesResult) GetSalary() int64 {
	if m != nil {
		return m.Salary
	}
	return 0
}

func (m *EmployeeWagesResult) GetLunchbreak() *EmployeeWagesResult_LunchBreak {
	if m != nil {
		return m.Lunchbreak
	}
	return nil
}

func (m *EmployeeWagesResult) GetBonus() *Bonus {
	if m != nil {
		return m.Bonus
	}
	return nil
}

func (m *EmployeeWagesResult) GetFees() *EmployeeWagesResult_WagesFees {
	if m != nil {
		return m.Fees
	}
	return nil
}

func (m *EmployeeWagesResult) GetTotalSalary() int64 {
	if m != nil {
		return m.TotalSalary
	}
	return 0
}

func (m *EmployeeWagesResult) GetSalaryToPay() int64 {
	if m != nil {
		return m.SalaryToPay
	}
	return 0
}

func (m *EmployeeWagesResult) GetSalaryCost() int64 {
	if m != nil {
		return m.SalaryCost
	}
	return 0
}

type EmployeeWagesResult_LunchBreak struct {
	Duration             int64    `protobuf:"varint,1,opt,name=duration,proto3" json:"duration,omitempty"`
	Reduction            int64    `protobuf:"varint,2,opt,name=reduction,proto3" json:"reduction,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *EmployeeWagesResult_LunchBreak) Reset()         { *m = EmployeeWagesResult_LunchBreak{} }
func (m *EmployeeWagesResult_LunchBreak) String() string { return proto.CompactTextString(m) }
func (*EmployeeWagesResult_LunchBreak) ProtoMessage()    {}
func (*EmployeeWagesResult_LunchBreak) Descriptor() ([]byte, []int) {
	return fileDescriptor_wages_096eba28521780f5, []int{6, 0}
}
func (m *EmployeeWagesResult_LunchBreak) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_EmployeeWagesResult_LunchBreak.Unmarshal(m, b)
}
func (m *EmployeeWagesResult_LunchBreak) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_EmployeeWagesResult_LunchBreak.Marshal(b, m, deterministic)
}
func (dst *EmployeeWagesResult_LunchBreak) XXX_Merge(src proto.Message) {
	xxx_messageInfo_EmployeeWagesResult_LunchBreak.Merge(dst, src)
}
func (m *EmployeeWagesResult_LunchBreak) XXX_Size() int {
	return xxx_messageInfo_EmployeeWagesResult_LunchBreak.Size(m)
}
func (m *EmployeeWagesResult_LunchBreak) XXX_DiscardUnknown() {
	xxx_messageInfo_EmployeeWagesResult_LunchBreak.DiscardUnknown(m)
}

var xxx_messageInfo_EmployeeWagesResult_LunchBreak proto.InternalMessageInfo

func (m *EmployeeWagesResult_LunchBreak) GetDuration() int64 {
	if m != nil {
		return m.Duration
	}
	return 0
}

func (m *EmployeeWagesResult_LunchBreak) GetReduction() int64 {
	if m != nil {
		return m.Reduction
	}
	return 0
}

type EmployeeWagesResult_WagesFees struct {
	HolidayAllowance     int64    `protobuf:"varint,1,opt,name=HolidayAllowance,proto3" json:"HolidayAllowance,omitempty"`
	Pension              int64    `protobuf:"varint,2,opt,name=Pension,proto3" json:"Pension,omitempty"`
	UnionFees            int64    `protobuf:"varint,3,opt,name=UnionFees,proto3" json:"UnionFees,omitempty"`
	SocialCost           int64    `protobuf:"varint,4,opt,name=SocialCost,proto3" json:"SocialCost,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *EmployeeWagesResult_WagesFees) Reset()         { *m = EmployeeWagesResult_WagesFees{} }
func (m *EmployeeWagesResult_WagesFees) String() string { return proto.CompactTextString(m) }
func (*EmployeeWagesResult_WagesFees) ProtoMessage()    {}
func (*EmployeeWagesResult_WagesFees) Descriptor() ([]byte, []int) {
	return fileDescriptor_wages_096eba28521780f5, []int{6, 1}
}
func (m *EmployeeWagesResult_WagesFees) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_EmployeeWagesResult_WagesFees.Unmarshal(m, b)
}
func (m *EmployeeWagesResult_WagesFees) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_EmployeeWagesResult_WagesFees.Marshal(b, m, deterministic)
}
func (dst *EmployeeWagesResult_WagesFees) XXX_Merge(src proto.Message) {
	xxx_messageInfo_EmployeeWagesResult_WagesFees.Merge(dst, src)
}
func (m *EmployeeWagesResult_WagesFees) XXX_Size() int {
	return xxx_messageInfo_EmployeeWagesResult_WagesFees.Size(m)
}
func (m *EmployeeWagesResult_WagesFees) XXX_DiscardUnknown() {
	xxx_messageInfo_EmployeeWagesResult_WagesFees.DiscardUnknown(m)
}

var xxx_messageInfo_EmployeeWagesResult_WagesFees proto.InternalMessageInfo

func (m *EmployeeWagesResult_WagesFees) GetHolidayAllowance() int64 {
	if m != nil {
		return m.HolidayAllowance
	}
	return 0
}

func (m *EmployeeWagesResult_WagesFees) GetPension() int64 {
	if m != nil {
		return m.Pension
	}
	return 0
}

func (m *EmployeeWagesResult_WagesFees) GetUnionFees() int64 {
	if m != nil {
		return m.UnionFees
	}
	return 0
}

func (m *EmployeeWagesResult_WagesFees) GetSocialCost() int64 {
	if m != nil {
		return m.SocialCost
	}
	return 0
}

type Employee struct {
	Id                   int32    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Employee) Reset()         { *m = Employee{} }
func (m *Employee) String() string { return proto.CompactTextString(m) }
func (*Employee) ProtoMessage()    {}
func (*Employee) Descriptor() ([]byte, []int) {
	return fileDescriptor_wages_096eba28521780f5, []int{7}
}
func (m *Employee) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Employee.Unmarshal(m, b)
}
func (m *Employee) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Employee.Marshal(b, m, deterministic)
}
func (dst *Employee) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Employee.Merge(dst, src)
}
func (m *Employee) XXX_Size() int {
	return xxx_messageInfo_Employee.Size(m)
}
func (m *Employee) XXX_DiscardUnknown() {
	xxx_messageInfo_Employee.DiscardUnknown(m)
}

var xxx_messageInfo_Employee proto.InternalMessageInfo

func (m *Employee) GetId() int32 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *Employee) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

type Department struct {
	Id                   int32    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Department) Reset()         { *m = Department{} }
func (m *Department) String() string { return proto.CompactTextString(m) }
func (*Department) ProtoMessage()    {}
func (*Department) Descriptor() ([]byte, []int) {
	return fileDescriptor_wages_096eba28521780f5, []int{8}
}
func (m *Department) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Department.Unmarshal(m, b)
}
func (m *Department) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Department.Marshal(b, m, deterministic)
}
func (dst *Department) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Department.Merge(dst, src)
}
func (m *Department) XXX_Size() int {
	return xxx_messageInfo_Department.Size(m)
}
func (m *Department) XXX_DiscardUnknown() {
	xxx_messageInfo_Department.DiscardUnknown(m)
}

var xxx_messageInfo_Department proto.InternalMessageInfo

func (m *Department) GetId() int32 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *Department) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

type EmployeeTask struct {
	Id                   int32    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *EmployeeTask) Reset()         { *m = EmployeeTask{} }
func (m *EmployeeTask) String() string { return proto.CompactTextString(m) }
func (*EmployeeTask) ProtoMessage()    {}
func (*EmployeeTask) Descriptor() ([]byte, []int) {
	return fileDescriptor_wages_096eba28521780f5, []int{9}
}
func (m *EmployeeTask) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_EmployeeTask.Unmarshal(m, b)
}
func (m *EmployeeTask) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_EmployeeTask.Marshal(b, m, deterministic)
}
func (dst *EmployeeTask) XXX_Merge(src proto.Message) {
	xxx_messageInfo_EmployeeTask.Merge(dst, src)
}
func (m *EmployeeTask) XXX_Size() int {
	return xxx_messageInfo_EmployeeTask.Size(m)
}
func (m *EmployeeTask) XXX_DiscardUnknown() {
	xxx_messageInfo_EmployeeTask.DiscardUnknown(m)
}

var xxx_messageInfo_EmployeeTask proto.InternalMessageInfo

func (m *EmployeeTask) GetId() int32 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *EmployeeTask) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

type EmployeeShift struct {
	Id                   int32                `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Checkin              *timestamp.Timestamp `protobuf:"bytes,2,opt,name=checkin,proto3" json:"checkin,omitempty"`
	Checkout             *timestamp.Timestamp `protobuf:"bytes,3,opt,name=checkout,proto3" json:"checkout,omitempty"`
	XXX_NoUnkeyedLiteral struct{}             `json:"-"`
	XXX_unrecognized     []byte               `json:"-"`
	XXX_sizecache        int32                `json:"-"`
}

func (m *EmployeeShift) Reset()         { *m = EmployeeShift{} }
func (m *EmployeeShift) String() string { return proto.CompactTextString(m) }
func (*EmployeeShift) ProtoMessage()    {}
func (*EmployeeShift) Descriptor() ([]byte, []int) {
	return fileDescriptor_wages_096eba28521780f5, []int{10}
}
func (m *EmployeeShift) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_EmployeeShift.Unmarshal(m, b)
}
func (m *EmployeeShift) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_EmployeeShift.Marshal(b, m, deterministic)
}
func (dst *EmployeeShift) XXX_Merge(src proto.Message) {
	xxx_messageInfo_EmployeeShift.Merge(dst, src)
}
func (m *EmployeeShift) XXX_Size() int {
	return xxx_messageInfo_EmployeeShift.Size(m)
}
func (m *EmployeeShift) XXX_DiscardUnknown() {
	xxx_messageInfo_EmployeeShift.DiscardUnknown(m)
}

var xxx_messageInfo_EmployeeShift proto.InternalMessageInfo

func (m *EmployeeShift) GetId() int32 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *EmployeeShift) GetCheckin() *timestamp.Timestamp {
	if m != nil {
		return m.Checkin
	}
	return nil
}

func (m *EmployeeShift) GetCheckout() *timestamp.Timestamp {
	if m != nil {
		return m.Checkout
	}
	return nil
}

type Bonus struct {
	Id                   string          `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string          `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Type                 Bonus_BonusType `protobuf:"varint,3,opt,name=type,proto3,enum=service.Bonus_BonusType" json:"type,omitempty"`
	XXX_NoUnkeyedLiteral struct{}        `json:"-"`
	XXX_unrecognized     []byte          `json:"-"`
	XXX_sizecache        int32           `json:"-"`
}

func (m *Bonus) Reset()         { *m = Bonus{} }
func (m *Bonus) String() string { return proto.CompactTextString(m) }
func (*Bonus) ProtoMessage()    {}
func (*Bonus) Descriptor() ([]byte, []int) {
	return fileDescriptor_wages_096eba28521780f5, []int{11}
}
func (m *Bonus) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Bonus.Unmarshal(m, b)
}
func (m *Bonus) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Bonus.Marshal(b, m, deterministic)
}
func (dst *Bonus) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Bonus.Merge(dst, src)
}
func (m *Bonus) XXX_Size() int {
	return xxx_messageInfo_Bonus.Size(m)
}
func (m *Bonus) XXX_DiscardUnknown() {
	xxx_messageInfo_Bonus.DiscardUnknown(m)
}

var xxx_messageInfo_Bonus proto.InternalMessageInfo

func (m *Bonus) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *Bonus) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Bonus) GetType() Bonus_BonusType {
	if m != nil {
		return m.Type
	}
	return Bonus_UNKNOWN
}

func init() {
	proto.RegisterType((*EmployeeWagesRequest)(nil), "service.EmployeeWagesRequest")
	proto.RegisterType((*EmployeeWagesResponse)(nil), "service.EmployeeWagesResponse")
	proto.RegisterType((*WagesRequest)(nil), "service.WagesRequest")
	proto.RegisterType((*WagesResponse)(nil), "service.WagesResponse")
	proto.RegisterType((*WagesResult)(nil), "service.WagesResult")
	proto.RegisterType((*Interval)(nil), "service.Interval")
	proto.RegisterType((*EmployeeWagesResult)(nil), "service.EmployeeWagesResult")
	proto.RegisterType((*EmployeeWagesResult_LunchBreak)(nil), "service.EmployeeWagesResult.LunchBreak")
	proto.RegisterType((*EmployeeWagesResult_WagesFees)(nil), "service.EmployeeWagesResult.WagesFees")
	proto.RegisterType((*Employee)(nil), "service.Employee")
	proto.RegisterType((*Department)(nil), "service.Department")
	proto.RegisterType((*EmployeeTask)(nil), "service.EmployeeTask")
	proto.RegisterType((*EmployeeShift)(nil), "service.EmployeeShift")
	proto.RegisterType((*Bonus)(nil), "service.Bonus")
	proto.RegisterEnum("service.EmployeeWagesResult_WagesType", EmployeeWagesResult_WagesType_name, EmployeeWagesResult_WagesType_value)
	proto.RegisterEnum("service.Bonus_BonusType", Bonus_BonusType_name, Bonus_BonusType_value)
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// WagesServiceClient is the client API for WagesService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type WagesServiceClient interface {
	EmployeeWages(ctx context.Context, in *EmployeeWagesRequest, opts ...grpc.CallOption) (*EmployeeWagesResponse, error)
	Wages(ctx context.Context, in *WagesRequest, opts ...grpc.CallOption) (*WagesResponse, error)
}

type wagesServiceClient struct {
	cc *grpc.ClientConn
}

func NewWagesServiceClient(cc *grpc.ClientConn) WagesServiceClient {
	return &wagesServiceClient{cc}
}

func (c *wagesServiceClient) EmployeeWages(ctx context.Context, in *EmployeeWagesRequest, opts ...grpc.CallOption) (*EmployeeWagesResponse, error) {
	out := new(EmployeeWagesResponse)
	err := c.cc.Invoke(ctx, "/service.WagesService/EmployeeWages", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *wagesServiceClient) Wages(ctx context.Context, in *WagesRequest, opts ...grpc.CallOption) (*WagesResponse, error) {
	out := new(WagesResponse)
	err := c.cc.Invoke(ctx, "/service.WagesService/Wages", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// WagesServiceServer is the server API for WagesService service.
type WagesServiceServer interface {
	EmployeeWages(context.Context, *EmployeeWagesRequest) (*EmployeeWagesResponse, error)
	Wages(context.Context, *WagesRequest) (*WagesResponse, error)
}

func RegisterWagesServiceServer(s *grpc.Server, srv WagesServiceServer) {
	s.RegisterService(&_WagesService_serviceDesc, srv)
}

func _WagesService_EmployeeWages_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(EmployeeWagesRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WagesServiceServer).EmployeeWages(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/service.WagesService/EmployeeWages",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WagesServiceServer).EmployeeWages(ctx, req.(*EmployeeWagesRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _WagesService_Wages_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(WagesRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WagesServiceServer).Wages(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/service.WagesService/Wages",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WagesServiceServer).Wages(ctx, req.(*WagesRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _WagesService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "service.WagesService",
	HandlerType: (*WagesServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "EmployeeWages",
			Handler:    _WagesService_EmployeeWages_Handler,
		},
		{
			MethodName: "Wages",
			Handler:    _WagesService_Wages_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "wages.proto",
}

func init() { proto.RegisterFile("wages.proto", fileDescriptor_wages_096eba28521780f5) }

var fileDescriptor_wages_096eba28521780f5 = []byte{
	// 865 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xac, 0x55, 0xdd, 0x6f, 0x1b, 0x45,
	0x10, 0xef, 0x9d, 0x7d, 0xfe, 0x98, 0x73, 0x22, 0x33, 0x6d, 0xc2, 0xc9, 0x2a, 0xad, 0x75, 0x20,
	0x08, 0x51, 0x71, 0x2b, 0xf3, 0x21, 0x04, 0x4f, 0x04, 0x5c, 0x62, 0x9a, 0xda, 0xd1, 0xda, 0xa1,
	0xf4, 0x09, 0x6d, 0x7c, 0xeb, 0xe4, 0x94, 0xf3, 0xad, 0xb9, 0x5b, 0xb7, 0xf2, 0xdf, 0x80, 0x10,
	0xef, 0x88, 0xbf, 0x94, 0x27, 0xb4, 0x73, 0x5f, 0x76, 0x4c, 0x8c, 0x1f, 0xfa, 0x72, 0xba, 0x9d,
	0xf9, 0xcd, 0x6f, 0xe7, 0x37, 0xb3, 0xb3, 0x0b, 0xf6, 0x5b, 0x7e, 0x25, 0xe2, 0xce, 0x3c, 0x92,
	0x4a, 0x62, 0x35, 0x16, 0xd1, 0x1b, 0x7f, 0x22, 0x5a, 0x8f, 0xaf, 0xa4, 0xbc, 0x0a, 0xc4, 0x53,
	0x32, 0x5f, 0x2e, 0xa6, 0x4f, 0x95, 0x3f, 0x13, 0xb1, 0xe2, 0xb3, 0x79, 0x82, 0x74, 0xa7, 0xf0,
	0xa0, 0x37, 0x9b, 0x07, 0x72, 0x29, 0xc4, 0x2b, 0x4d, 0xc0, 0xc4, 0x6f, 0x0b, 0x11, 0x2b, 0xfc,
	0x0c, 0x6a, 0x7e, 0xa8, 0x44, 0xf4, 0x86, 0x07, 0x8e, 0xd1, 0x36, 0x8e, 0xec, 0xee, 0x7b, 0x9d,
	0x94, 0xb4, 0xd3, 0x4f, 0x1d, 0x2c, 0x87, 0xe0, 0x63, 0xb0, 0x45, 0x4a, 0xf3, 0xab, 0xef, 0x39,
	0x66, 0xdb, 0x38, 0xb2, 0x18, 0x64, 0xa6, 0xbe, 0xe7, 0xbe, 0x80, 0x83, 0x5b, 0xfb, 0xc4, 0x73,
	0x19, 0xc6, 0x02, 0xbb, 0x60, 0x51, 0xe6, 0x8e, 0xd1, 0x2e, 0x1d, 0xd9, 0xdd, 0x87, 0xf9, 0x2e,
	0xb7, 0xe1, 0x8b, 0x40, 0xb1, 0x04, 0xea, 0xfe, 0x6d, 0x40, 0xe3, 0x9d, 0x66, 0x5b, 0x5a, 0xcf,
	0x16, 0x3f, 0x84, 0x3d, 0x4f, 0xcc, 0x79, 0xa4, 0x66, 0x22, 0x54, 0x1a, 0x52, 0x22, 0x41, 0x8d,
	0xc2, 0xd8, 0xf7, 0xf0, 0x7d, 0xa8, 0x2a, 0x1e, 0xdf, 0x68, 0x77, 0x99, 0xdc, 0x15, 0xbd, 0xec,
	0x7b, 0xee, 0xb7, 0xb0, 0xb7, 0xae, 0xf1, 0x18, 0x2c, 0x4d, 0x9e, 0x69, 0x7c, 0x90, 0xe7, 0xb6,
	0xa6, 0x8d, 0x20, 0xee, 0x1c, 0xec, 0x15, 0xab, 0x56, 0x96, 0xe5, 0xb5, 0xa1, 0x2c, 0xab, 0x10,
	0xcb, 0x21, 0x45, 0x35, 0xcd, 0xdd, 0xab, 0x39, 0x85, 0x5a, 0x56, 0x23, 0xec, 0x40, 0x79, 0x1a,
	0xc9, 0x59, 0xba, 0x55, 0xab, 0x93, 0x1c, 0x9f, 0x4e, 0x76, 0x7c, 0x3a, 0xe3, 0xec, 0xf8, 0x30,
	0xc2, 0xe1, 0x31, 0x98, 0x4a, 0x52, 0xbb, 0xb7, 0xa3, 0x4d, 0x25, 0xdd, 0x7f, 0x2a, 0x70, 0xff,
	0x3f, 0xd2, 0xc0, 0x6f, 0xa0, 0xac, 0x96, 0xf3, 0x44, 0xde, 0x7e, 0xf7, 0xe3, 0x6d, 0x29, 0x27,
	0x05, 0x1b, 0x2f, 0xe7, 0x82, 0x51, 0x0c, 0x7e, 0x09, 0x76, 0xd1, 0x93, 0x38, 0x4d, 0xe4, 0x7e,
	0x4e, 0xf1, 0x43, 0xee, 0x63, 0xab, 0x38, 0xfc, 0x14, 0xca, 0xba, 0x57, 0xd4, 0x56, 0xbb, 0x7b,
	0xb0, 0xb1, 0xe5, 0x98, 0xc7, 0x37, 0x8c, 0x20, 0xba, 0x22, 0x1e, 0x57, 0x82, 0x5a, 0xfc, 0x3f,
	0x15, 0xd1, 0x38, 0x7c, 0x02, 0x56, 0x7c, 0xed, 0x4f, 0x95, 0x63, 0x51, 0xc0, 0xe1, 0x06, 0xf7,
	0x48, 0x7b, 0x59, 0x02, 0xc2, 0x16, 0xd4, 0xbc, 0x45, 0xc4, 0x95, 0x2f, 0x43, 0xa7, 0xd2, 0x36,
	0x8e, 0x4a, 0x2c, 0x5f, 0xe3, 0x21, 0x54, 0x62, 0x1e, 0xf0, 0x68, 0xe9, 0x54, 0xc9, 0x93, 0xae,
	0xf0, 0x47, 0x80, 0x60, 0x11, 0x4e, 0xae, 0x2f, 0x23, 0xc1, 0x6f, 0x9c, 0x1a, 0x6d, 0xf3, 0xc9,
	0xd6, 0xaa, 0x9d, 0x69, 0xf8, 0x89, 0x86, 0xb3, 0x95, 0x50, 0xfc, 0x08, 0xac, 0x4b, 0x19, 0x2e,
	0x62, 0xa7, 0x4e, 0x1c, 0xfb, 0x39, 0xc7, 0x89, 0xb6, 0xb2, 0xc4, 0xa9, 0xdb, 0x33, 0x15, 0x22,
	0x76, 0x80, 0x40, 0x3b, 0xb4, 0xe7, 0xb9, 0x10, 0x31, 0xa3, 0x18, 0x6c, 0x83, 0x3d, 0x96, 0x8a,
	0x07, 0xa3, 0x44, 0x87, 0x4d, 0x3a, 0x56, 0x4d, 0x1a, 0x91, 0xfc, 0x8d, 0xe5, 0x39, 0x5f, 0x3a,
	0x8d, 0x04, 0xb1, 0x62, 0xc2, 0x47, 0x00, 0xc9, 0xf2, 0x7b, 0x19, 0x2b, 0x67, 0x8f, 0x00, 0x2b,
	0x96, 0xd6, 0x73, 0x80, 0x42, 0xdf, 0x5a, 0x41, 0x8d, 0x5b, 0x05, 0x7d, 0x08, 0xf5, 0x48, 0x78,
	0x8b, 0x09, 0x39, 0x4d, 0x72, 0x16, 0x86, 0xd6, 0x9f, 0x06, 0xd4, 0xf3, 0xfc, 0xf1, 0x18, 0x9a,
	0xa7, 0x32, 0xf0, 0x3d, 0xbe, 0xfc, 0x2e, 0x08, 0xe4, 0x5b, 0x1e, 0x4e, 0x44, 0xca, 0xb7, 0x61,
	0x47, 0x07, 0xaa, 0xe7, 0x22, 0x8c, 0x0b, 0xd6, 0x6c, 0xa9, 0x77, 0xbc, 0x08, 0x7d, 0x19, 0x6a,
	0x4a, 0x3a, 0x6c, 0x25, 0x56, 0x18, 0x48, 0x99, 0x9c, 0xf8, 0x3c, 0x20, 0x65, 0xe5, 0x54, 0x59,
	0x6e, 0x71, 0x7f, 0x4a, 0x13, 0xd2, 0xe7, 0x1d, 0x6d, 0xa8, 0x5e, 0x0c, 0x5e, 0x0c, 0x86, 0xaf,
	0x06, 0xcd, 0x7b, 0x08, 0x50, 0x39, 0x1d, 0x5e, 0xb0, 0xb3, 0xd7, 0x4d, 0x43, 0x3b, 0x5e, 0x0e,
	0x07, 0xa7, 0xe3, 0xb3, 0xd7, 0x4d, 0x13, 0xeb, 0x60, 0x9d, 0x0c, 0x07, 0x17, 0xa3, 0x66, 0x09,
	0x1b, 0x50, 0x1b, 0xfe, 0xdc, 0x63, 0xe3, 0xfe, 0xcb, 0x5e, 0xb3, 0xec, 0x76, 0xa0, 0x96, 0x35,
	0x0c, 0xf7, 0xc1, 0xf4, 0x3d, 0x52, 0x63, 0x31, 0xd3, 0xf7, 0x10, 0xa1, 0x1c, 0xf2, 0x99, 0xa0,
	0xe4, 0xeb, 0x8c, 0xfe, 0xdd, 0x67, 0x00, 0xc5, 0xf0, 0xec, 0x14, 0xd1, 0x85, 0xc6, 0xea, 0xf8,
	0xec, 0x14, 0xf3, 0x87, 0x01, 0x7b, 0x6b, 0x73, 0xb1, 0x11, 0xf5, 0x05, 0x54, 0x27, 0xd7, 0x62,
	0x72, 0xe3, 0x87, 0x3b, 0xdc, 0x32, 0x19, 0x14, 0xbf, 0x82, 0x1a, 0xfd, 0xca, 0x85, 0x4a, 0x67,
	0x7c, 0x5b, 0x58, 0x8e, 0x75, 0x7f, 0x37, 0xc0, 0xa2, 0xc3, 0xbf, 0x92, 0x47, 0xfd, 0xae, 0xec,
	0xf1, 0x49, 0x7a, 0x71, 0x95, 0xe8, 0xe2, 0x72, 0xd6, 0xc7, 0x27, 0xf9, 0x16, 0x57, 0x95, 0xfb,
	0x0c, 0xea, 0xb9, 0x69, 0xbd, 0x9b, 0x79, 0xd3, 0xa8, 0x99, 0xbd, 0x5f, 0xce, 0x7b, 0x83, 0x51,
	0xaf, 0x69, 0x76, 0xff, 0xca, 0x9e, 0xb9, 0x51, 0x42, 0x8c, 0xe7, 0x45, 0xb5, 0xc8, 0x8e, 0x1f,
	0xdc, 0x35, 0x8d, 0xf4, 0x2c, 0xb6, 0x1e, 0xdd, 0x39, 0xac, 0xf4, 0x2e, 0xb9, 0xf7, 0xf0, 0x6b,
	0xb0, 0x12, 0xa6, 0x83, 0xdb, 0x6f, 0x52, 0xc2, 0x70, 0xb8, 0xf1, 0x54, 0xa5, 0x91, 0x97, 0x15,
	0x2a, 0xe4, 0xe7, 0xff, 0x06, 0x00, 0x00, 0xff, 0xff, 0x64, 0x4e, 0x27, 0x90, 0x78, 0x08, 0x00,
	0x00,
}
