package query

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	"time"

	mssql "github.com/denisenkom/go-mssqldb"
	"gitlab.com/stjarni/wages"
	"gitlab.com/stjarni/wages/core/amount"
)

type emplbonuses map[int][]*wages.Bonus

func (q *query) bonuses(ctx context.Context, p *wages.Period, emplids ...int32) (emplbonuses, error) {
	query := `select b.Id, b.UserID, b.StartDate, b.EndDate,
		b.Text, l.DepartmentId, l.TaskId, 
		b.BonusType, b.IsFixedAmount, l.Amount, b.Percentage
		from [Bonus] as b 
		inner join [BonusToDepartmentTask] as l on l.BonusId = b.Id
		where (l.Amount > 0 or b.Percentage > 0)
		and b.StartDate < @p1 and (b.EndDate is null or b.EndDate >= @p2)`

	args := []interface{}{p.To, p.From}
	if len(emplids) != 0 {
		var params []string
		for idx, id := range emplids {
			args = append(args, id)
			params = append(params, fmt.Sprintf("@p%d", idx+3))
		}
		query += fmt.Sprintf(" and b.UserId in (%s)", strings.Join(params, ","))
	}

	rows, err := q.db.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	type row struct {
		id           mssql.UniqueIdentifier
		userid       int
		text         string
		departmentid int32
		taskid       int32
		startdate    time.Time
		enddate      sql.NullString
		btype        int
		isfixed      bool
		amount       sql.NullFloat64
		percentage   sql.NullFloat64
	}

	result := make(map[int][]*wages.Bonus)
	for rows.Next() {
		r := row{}
		if err := rows.Scan(&r.id, &r.userid, &r.startdate, &r.enddate, &r.text, &r.departmentid, &r.taskid,
			&r.btype, &r.isfixed, &r.amount, &r.percentage); err != nil {
			return nil, err
		}
		bonus := wages.Bonus{ID: r.id.String(), Text: r.text, DepartmentID: r.departmentid, TaskID: r.taskid, StartDate: r.startdate}
		bonus.Type = wages.SalaryType(r.btype)
		if r.enddate.Valid {
			t, err := time.Parse(time.RFC3339, r.enddate.String)
			if err != nil {
				return nil, err
			}
			bonus.EndDate = t
		}
		if r.isfixed {
			bonus.AmountType = amount.AmountDKK
		} else {
			bonus.AmountType = amount.AmountPercentage
		}
		if r.amount.Valid {
			bonus.Amount = new(amount.DKK).SetFloat64(r.amount.Float64)
		}
		if r.percentage.Valid {
			bonus.Percentage = new(amount.Percentage).SetFloat64(r.percentage.Float64)
		}

		result[r.userid] = append(result[r.userid], &bonus)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return result, nil
}
