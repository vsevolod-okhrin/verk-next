package query

import (
	"context"
	"database/sql"
	"fmt"
	"math"
	"sort"
	"time"

	"gitlab.com/stjarni/wages"
	"gitlab.com/stjarni/wages/core/amount"
)

func find(rs []*wages.CompanyPolicy, date time.Time) *wages.CompanyPolicy {
	for _, r := range rs {
		if r.Date.After(date) {
			continue
		} else {
			return r
		}
	}
	panic(fmt.Errorf("query: company policy not found for %s", date.Format("2006-01-02")))
}

func collect(p *wages.Period, revset ...[]*wages.CompanyPolicy) []time.Time {
	dateset := make(map[time.Time]struct{})
	dateset[p.From] = struct{}{}

	for _, revs := range revset {
		for _, r := range revs {
			if r.Date.After(p.From) && !r.Date.After(p.To) {
				dateset[r.Date] = struct{}{}
			}
		}
	}

	var dates []time.Time
	for d := range dateset {
		dates = append(dates, d)
	}

	sort.Slice(dates, func(i, j int) bool {
		return dates[i].After(dates[j])
	})
	return dates
}

func (q *query) fetchCompanySettings(ctx context.Context) ([]*wages.CompanyPolicy, error) {
	rows, err := q.db.QueryContext(ctx, `select cs.FromDate,
		cs.ShiftsGapMaxMinutes, cs.JoinDepartmentsForLunchBreak
		from [CompanySettings] as cs
		order by cs.FromDate desc
	`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	type row struct {
		date     time.Time
		shiftgap sql.NullFloat64
		depjoin  sql.NullBool
	}

	var result []*wages.CompanyPolicy
	for rows.Next() {
		r := row{}
		if err := rows.Scan(&r.date, &r.shiftgap, &r.depjoin); err != nil {
			return nil, err
		}
		res := &wages.CompanyPolicy{Date: r.date, LunchBreak: new(wages.LunchBreakCompanyPolicy)}
		if r.shiftgap.Valid {
			res.LunchBreak.ShiftGap = int(math.Floor(r.shiftgap.Float64))
		}
		if r.depjoin.Valid {
			res.LunchBreak.AllowDepartmentJoining = r.depjoin.Bool
		}
		result = append(result, res)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return result, nil
}

func (q *query) fetchHolidayAllowance(ctx context.Context) ([]*wages.CompanyPolicy, error) {
	rows, err := q.db.QueryContext(ctx, `select ha.FromDate,
		ha.FixedSalaryPercentage, ha.HourlySalaryPercentage
		from [PublicDutyHolidayAllowance] as ha
		order by ha.FromDate desc`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	type row struct {
		date      time.Time
		ratefixed sql.NullFloat64
		ratehoury sql.NullFloat64
	}

	var result []*wages.CompanyPolicy
	for rows.Next() {
		r := row{}
		err := rows.Scan(&r.date, &r.ratefixed, &r.ratehoury)
		if err != nil {
			return nil, err
		}
		res := &wages.CompanyPolicy{Date: r.date, HolidayAllowance: new(wages.HolidayAllowancePolicy)}
		if r.ratefixed.Valid && r.ratehoury.Valid {
			res.HolidayAllowance.RateFixed = new(amount.Percentage).SetFloat64(r.ratefixed.Float64 / 100.0)
			res.HolidayAllowance.RateHourly = new(amount.Percentage).SetFloat64(r.ratehoury.Float64 / 100.0)
		}
		result = append(result, res)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return result, nil
}

func (q *query) fetchPublicPension(ctx context.Context) ([]*wages.CompanyPolicy, error) {
	rows, err := q.db.QueryContext(ctx, `select pd.FromDate,
		pd.PensionPercentage, pd.PensionMinAge, pd.PensionMaxAge
		from [PublicDutyPension] as pd
		order by pd.FromDate desc`)
	if err != nil {
		return nil, err
	}

	type row struct {
		date   time.Time
		rate   float64
		agemin int
		agemax int
	}

	var result []*wages.CompanyPolicy
	for rows.Next() {
		r := row{}
		err := rows.Scan(&r.date, &r.rate, &r.agemin, &r.agemax)
		if err != nil {
			return nil, err
		}
		res := &wages.CompanyPolicy{Date: r.date, PublicPension: &wages.PensionPolicy{
			Rate:   new(amount.Percentage).SetFloat64(r.rate / 100.0),
			AgeMin: r.agemin,
			AgeMax: r.agemax,
		}}
		result = append(result, res)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return result, nil
}

func (q *query) fetchAmeg(ctx context.Context) ([]*wages.CompanyPolicy, error) {
	rows, err := q.db.QueryContext(ctx, `select pd.FromDate, pd.Percentage
		from [PublicDutyAmeg] as pd
		order by pd.FromDate desc`)
	if err != nil {
		return nil, err
	}

	type row struct {
		date time.Time
		rate float64
	}

	var result []*wages.CompanyPolicy
	for rows.Next() {
		r := row{}
		if err := rows.Scan(&r.date, &r.rate); err != nil {
			return nil, err
		}
		res := &wages.CompanyPolicy{Date: r.date, Ameg: &wages.PublicDutyPolicy{
			Rate: new(amount.Percentage).SetFloat64(r.rate / 100.0),
		}}
		result = append(result, res)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return result, nil
}

func (q *query) fetchBarsilsgjald(ctx context.Context) ([]*wages.CompanyPolicy, error) {
	rows, err := q.db.QueryContext(ctx, `select pd.FromDate, pd.Percentage
		from [PublicDutyBarsilsgjald] as pd
		order by pd.FromDate desc`)
	if err != nil {
		return nil, err
	}

	type row struct {
		date time.Time
		rate float64
	}

	var result []*wages.CompanyPolicy
	for rows.Next() {
		r := row{}
		if err := rows.Scan(&r.date, &r.rate); err != nil {
			return nil, err
		}
		res := &wages.CompanyPolicy{Date: r.date, Barsilsgjald: &wages.PublicDutyPolicy{
			Rate: new(amount.Percentage).SetFloat64(r.rate / 100.0),
		}}
		result = append(result, res)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return result, nil
}

func (q *query) fetchAls(ctx context.Context) ([]*wages.CompanyPolicy, error) {
	rows, err := q.db.QueryContext(ctx, `select pd.FromDate, pd.Percentage
		from [PublicDutyUnemploymentContribution] as pd
		order by pd.FromDate desc`)
	if err != nil {
		return nil, err
	}

	type row struct {
		date time.Time
		rate float64
	}

	var result []*wages.CompanyPolicy
	for rows.Next() {
		r := row{}
		if err := rows.Scan(&r.date, &r.rate); err != nil {
			return nil, err
		}
		res := &wages.CompanyPolicy{Date: r.date, Als: &wages.PublicDutyPolicy{
			Rate: new(amount.Percentage).SetFloat64(r.rate / 100.0),
		}}
		result = append(result, res)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return result, nil
}
