package query

import (
	"context"
	"database/sql"
	"time"

	"gitlab.com/stjarni/wages"
)

type query struct {
	db *sql.DB

	salaryscales map[int32]*salaryscale
	departments  map[int32]*department
	unions       map[int32]*union
	localdeps    map[int32]*localdep
}

// New creates new instance of query service
func New(db *sql.DB) wages.Query {
	return &query{
		db: db,

		salaryscales: make(map[int32]*salaryscale),
		departments:  make(map[int32]*department),
		unions:       make(map[int32]*union),
		localdeps:    make(map[int32]*localdep),
	}
}

func (q *query) Employees(ctx context.Context, p *wages.Period, ids ...int32) ([]*wages.Employee, error) {
	bonuses, err := q.bonuses(ctx, p, ids...)
	if err != nil {
		return nil, err
	}
	empls, err := q.employees(ctx, ids...)
	if err != nil {
		return nil, err
	}
	return q.emplrevisions(ctx, bonuses, empls, p)
}

func (q *query) ScanEmployees(ctx context.Context, p *wages.Period) ([]*wages.Employee, error) {
	bonuses, err := q.bonuses(ctx, p)
	if err != nil {
		return nil, err
	}
	empls, err := q.employees(ctx)
	if err != nil {
		return nil, err
	}
	return q.emplrevisions(ctx, bonuses, empls, p)
}

func (q *query) CompanyPolicies(ctx context.Context, period *wages.Period) ([]*wages.CompanyPolicy, error) {
	settings, err := q.fetchCompanySettings(ctx)
	if err != nil {
		return nil, err
	}
	haPolicy, err := q.fetchHolidayAllowance(ctx)
	if err != nil {
		return nil, err
	}
	penPolicy, err := q.fetchPublicPension(ctx)
	if err != nil {
		return nil, err
	}
	ameg, err := q.fetchAmeg(ctx)
	if err != nil {
		return nil, err
	}
	barsilsgjald, err := q.fetchBarsilsgjald(ctx)
	if err != nil {
		return nil, err
	}
	als, err := q.fetchAls(ctx)
	if err != nil {
		return nil, err
	}

	dates := collect(period, settings, haPolicy, penPolicy, ameg, barsilsgjald, als)
	var result []*wages.CompanyPolicy
	for _, d := range dates {
		r := &wages.CompanyPolicy{Date: d}
		r.LunchBreak = find(settings, d).LunchBreak
		r.HolidayAllowance = find(haPolicy, d).HolidayAllowance
		r.PublicPension = find(penPolicy, d).PublicPension
		r.Ameg = find(ameg, d).Ameg
		r.Barsilsgjald = find(barsilsgjald, d).Barsilsgjald
		r.Als = find(als, d).Als
		result = append(result, r)
	}
	return result, nil
}

func (q *query) Periods(ctx context.Context, p *wages.Period) ([]*wages.WagesPeriod, error) {
	rows, err := q.db.QueryContext(ctx, `select
		HourlyFromDate, HourlyToDate, FixedFromDate, FixedToDate, PaymentDate
		from PaymentPeriod
		where Status = 1 and 
		(HourlyFromDate < @p2 or FixedFromDate < @p2) and
		(HourlyToDate >= @p1 or FixedToDate >= @p1)
		order by FixedFromDate desc`, p.From, p.To)
	if err != nil {
		return nil, err
	}

	type row struct {
		hfrom time.Time
		hto   time.Time
		mfrom time.Time
		mto   time.Time
		calc  time.Time
	}

	var result []*wages.WagesPeriod
	for rows.Next() {
		r := row{}
		if err := rows.Scan(&r.hfrom, &r.hto, &r.mfrom, &r.mto, &r.calc); err != nil {
			return nil, err
		}
		result = append(result, &wages.WagesPeriod{
			Hourly:   &wages.Period{From: r.hfrom, To: r.hto.Add(time.Duration(24 * time.Hour))},
			Monthly:  &wages.Period{From: r.mfrom, To: r.mto.Add(time.Duration(24 * time.Hour))},
			CalcDate: r.calc,
		})
	}
	return result, nil
}
