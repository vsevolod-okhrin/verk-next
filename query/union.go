package query

import (
	"context"
	"database/sql"
	"time"

	"gitlab.com/stjarni/wages"
	"gitlab.com/stjarni/wages/core/amount"
)

type union struct {
	entity    *wages.Union
	revisions unionrevs
}

type unionrevs []*wages.UnionRevision

func (revs *unionrevs) period(period *wages.Period) []*wages.UnionRevision {
	var result []*wages.UnionRevision
	for _, r := range *revs {
		if r.Date.After(period.To) || r.Date.Equal(period.To) {
			continue
		}
		if r.Date.After(period.From) {
			result = append(result, r)
		} else {
			result = append(result, r)
			break
		}
	}
	return result
}

func (revs *unionrevs) date(date time.Time) *wages.UnionRevision {
	for _, r := range *revs {
		if r.Date.After(date) {
			continue
		} else {
			return r
		}
	}
	return nil
}

func (q *query) union(ctx context.Context, id int32) (*union, error) {
	rows, err := q.db.QueryContext(ctx, `select 
			u.Name, u.Email, u.Phone, uh.FromDate,
			uh.MembershipFeeRate, uh.IsMembershipFeePaidByEmployer, uh.MembershipFeeMax,
			uh.MembershipFeeAccount, mb.RegNo, mb.Name,
			uh.InsuranceRate, uh.IsInsurancePaidByEmployer, uh.InsuranceMax,
			uh.InsuranceAccount, ib.RegNo, ib.Name,
			uh.PensionRateByEmployee, uh.PensionRateByEmployer,
			uh.PensionAccount, pb.RegNo, pb.Name,
			uh.HolidayAllowanceAccount, hb.RegNo, hb.Name
		from [Union] as u 
		inner join [UnionHistory] as uh on uh.UnionId = u.Id
		left join [Bank] as mb on mb.RegNo = uh.MembershipFeeBankId
		left join [Bank] as ib on ib.RegNo = uh.InsuranceBankId
		left join [Bank] as pb on pb.RegNo = uh.PensionBankId
		left join [Bank] as hb on hb.RegNo = uh.HolidayAllowanceBankId
		where uh.UnionId = @p1
		order by FromDate desc
	`, id)

	type row struct {
		name        string
		email       sql.NullString
		phone       sql.NullString
		date        time.Time
		mrate       sql.NullFloat64
		mbyemployer sql.NullBool
		mmax        sql.NullFloat64
		maccount    sql.NullString
		mbankreg    sql.NullString
		mbankname   sql.NullString
		irate       sql.NullFloat64
		ibyemployer sql.NullBool
		imax        sql.NullFloat64
		iaccount    sql.NullString
		ibankreg    sql.NullString
		ibankname   sql.NullString
		prateemplr  sql.NullFloat64
		prateemple  sql.NullFloat64
		paccount    sql.NullString
		pbankreg    sql.NullString
		pbankname   sql.NullString
		haccount    sql.NullString
		hbankreg    sql.NullString
		hbankname   sql.NullString
	}

	if err != nil {
		return nil, err
	}

	var result *union
	for rows.Next() {
		r := row{}
		if rows.Scan(&r.name, &r.email, &r.phone, &r.date,
			&r.mrate, &r.mbyemployer, &r.mmax, &r.maccount, &r.mbankreg, &r.mbankname,
			&r.irate, &r.ibyemployer, &r.imax, &r.iaccount, &r.ibankreg, &r.ibankname,
			&r.prateemple, &r.prateemplr, &r.paccount, &r.pbankreg, &r.pbankname,
			&r.haccount, &r.hbankreg, &r.hbankname); err != nil {
			return nil, err
		}

		if result == nil {
			result = &union{entity: &wages.Union{ID: id, Name: r.name}}
			if r.email.Valid {
				result.entity.Email = r.email.String
			}
			if r.phone.Valid {
				result.entity.Phone = r.phone.String
			}
		}

		val := wages.UnionRevision{Date: r.date}

		if r.mrate.Valid {
			rates := wages.FeeRates{}
			rate := new(amount.Percentage).SetFloat64(r.mrate.Float64 / 100.0)
			if r.mbyemployer.Bool {
				rates.RateByEmployer = rate
			} else {
				rates.RateByEmployee = rate
			}
			if r.mmax.Valid {
				rates.Maximum = new(amount.DKK).SetFloat64(r.mrate.Float64)
			}
			val.MembershipFee = &rates
		}
		if r.mbankreg.Valid {
			val.MembershipFeeAccount = &wages.BankAccount{
				AccountNo: r.maccount.String,
				RegNo:     r.mbankreg.String,
				BankName:  r.mbankname.String,
			}
		}
		if r.irate.Valid {
			rates := wages.FeeRates{}
			rate := new(amount.Percentage).SetFloat64(r.irate.Float64 / 100.0)
			if r.ibyemployer.Bool {
				rates.RateByEmployer = rate
			} else {
				rates.RateByEmployee = rate
			}
			if r.imax.Valid {
				rates.Maximum = new(amount.DKK).SetFloat64(r.imax.Float64)
			}
			val.InsuranceFee = &rates
		}
		if r.ibankreg.Valid {
			val.InsuranceFeeAccount = &wages.BankAccount{
				AccountNo: r.iaccount.String,
				RegNo:     r.ibankreg.String,
				BankName:  r.ibankname.String,
			}
		}
		if r.prateemple.Valid || r.prateemplr.Valid {
			rates := wages.PensionRates{}
			if r.prateemple.Valid {
				rates.RateByEmployee = new(amount.Percentage).SetFloat64(r.prateemple.Float64 / 100.0)
			}
			if r.prateemplr.Valid {
				rates.RateByEmployer = new(amount.Percentage).SetFloat64(r.prateemplr.Float64 / 100.0)
			}
			val.Pension = &rates
		}
		if r.pbankreg.Valid {
			val.PensionAccount = &wages.BankAccount{
				AccountNo: r.paccount.String,
				RegNo:     r.pbankreg.String,
				BankName:  r.pbankname.String,
			}
		}
		if r.hbankreg.Valid {
			val.HolidayAllowanceAccount = &wages.BankAccount{
				AccountNo: r.haccount.String,
				RegNo:     r.hbankreg.String,
				BankName:  r.hbankname.String,
			}
		}

		result.revisions = append(result.revisions, &val)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return result, nil
}
