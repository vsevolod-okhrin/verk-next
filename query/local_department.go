package query

import (
	"context"
	"database/sql"
	"time"

	"gitlab.com/stjarni/wages"
	"gitlab.com/stjarni/wages/core/amount"
)

type localdep struct {
	entity    *wages.LocalDepartment
	revisions localdeprevs
}

type localdeprev struct {
	Date                    time.Time
	MembershipFee           *wages.FeeRates
	MembershipFeeAccount    *wages.BankAccount
	HolidayAllowanceAccount *wages.BankAccount
	UnionID                 int32
}

type localdeprevs []*localdeprev

func (revs *localdeprevs) period(period *wages.Period) []*localdeprev {
	var result []*localdeprev
	for _, r := range *revs {
		if r.Date.After(period.To) || r.Date.Equal(period.To) {
			continue
		}
		if r.Date.After(period.From) {
			result = append(result, r)
		} else {
			result = append(result, r)
			break
		}
	}
	return result
}

func (revs *localdeprevs) date(date time.Time) *localdeprev {
	for _, r := range *revs {
		if r.Date.After(date) {
			continue
		} else {
			return r
		}
	}
	return nil
}

func (q *query) localdep(ctx context.Context, id int32) (*localdep, error) {
	rows, err := q.db.QueryContext(ctx, `select ld.Name, ldh.FromDate,
		uh.UnionId, ldh.MembershipFeeRate, ldh.IsMembershipFeePaidByEmployer, ldh.MembershipFeeMax,
		ldh.MembershipFeeAccount, mb.RegNo, mb.Name, ldh.HolidayAllowanceAccount, hb.RegNo, hb.Name
	from LocalDepartment as ld
	inner join LocalDepartmentHistory as ldh on ldh.LocalDepartmentId = ld.Id
	inner join LocalDepartmentToUnionHistory as lduh on lduh.LocalDepartmentId = ld.Id
	inner join UnionHistory as uh on uh.Id = lduh.UnionHistoryId and uh.FromDate = ldh.FromDate
	left join Bank as mb on mb.RegNo = ldh.MembershipFeeBankId
	left join Bank as hb on hb.RegNo = ldh.HolidayAllowanceBankId
	where ld.Id = @p1
	order by ldh.FromDate desc`, id)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	type row struct {
		name        string
		date        time.Time
		unionid     int32
		mrate       sql.NullFloat64
		mbyemployer sql.NullBool
		mmax        sql.NullFloat64
		maccount    sql.NullString
		mbankreg    sql.NullString
		mbankname   sql.NullString
		haccount    sql.NullString
		hbankreg    sql.NullString
		hbankname   sql.NullString
	}
	var result *localdep
	for rows.Next() {
		r := row{}
		if err := rows.Scan(&r.name, &r.date, &r.unionid,
			&r.mrate, &r.mbyemployer, &r.mmax, &r.maccount, &r.mbankreg, &r.mbankname,
			&r.haccount, &r.hbankreg, &r.hbankname); err != nil {
			return nil, err
		}

		if result == nil {
			result = &localdep{entity: &wages.LocalDepartment{ID: id, Name: r.name}}
		}

		rev := localdeprev{Date: r.date, UnionID: r.unionid}
		if r.mrate.Valid {
			rates := wages.FeeRates{}
			rate := new(amount.Percentage).SetFloat64(r.mrate.Float64 / 100.0)
			if r.mbyemployer.Bool {
				rates.RateByEmployer = rate
			} else {
				rates.RateByEmployee = rate
			}
			if r.mmax.Valid {
				rates.Maximum = new(amount.DKK).SetFloat64(r.mrate.Float64)
			}
			rev.MembershipFee = &rates
		}
		if r.mbankreg.Valid {
			rev.MembershipFeeAccount = &wages.BankAccount{
				AccountNo: r.maccount.String,
				RegNo:     r.mbankreg.String,
				BankName:  r.mbankname.String,
			}
		}
		if r.hbankreg.Valid {
			rev.HolidayAllowanceAccount = &wages.BankAccount{
				AccountNo: r.haccount.String,
				RegNo:     r.hbankreg.String,
				BankName:  r.hbankname.String,
			}
		}
		result.revisions = append(result.revisions, &rev)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return result, nil

}
