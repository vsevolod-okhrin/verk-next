package query

import (
	"context"
	"database/sql"
	"strconv"
	"time"

	"gitlab.com/stjarni/wages"
	"gitlab.com/stjarni/wages/core/amount"
)

type salaryscale struct {
	Entity    *wages.SalaryScale
	Revisions salaryscalerevs
}

type salaryscalerevs []*wages.SalaryScaleRevision

func (revs *salaryscalerevs) period(period *wages.Period) []*wages.SalaryScaleRevision {
	var result []*wages.SalaryScaleRevision
	for _, r := range *revs {
		if r.Date.After(period.To) || r.Date.Equal(period.To) {
			continue
		}
		if r.Date.After(period.From) {
			result = append(result, r)
		} else {
			result = append(result, r)
			break
		}
	}
	return result
}

func (revs *salaryscalerevs) date(date time.Time) *wages.SalaryScaleRevision {
	for _, r := range *revs {
		if r.Date.After(date) {
			continue
		} else {
			return r
		}
	}
	return nil
}

func (q *query) salaryscale(ctx context.Context, id int32) (*salaryscale, error) {
	rows, err := q.db.Query(`select 
	      ss.ID, ss.Name, ss.SalaryScaleType,
		  ssh.FromDate, sshf.FromDate, 
		  sshf.PensionRateByEmployee, sshf.PensionRateByEmployer,
		  ssh.BasicSalary, ssh.PensionRateByEmployee, ssh.PensionRateByEmployer,
		  ssr.Name, ssr.RateSalary,
		  ssrspec.DayOfWeek, ssrspec.FromMinutes, ssrspec.ToMinutes
		from [SalaryScale] as ss
		left join [SalaryScaleHourlyHistory] as ssh on ssh.SalaryScaleId = ss.Id
		left join [SalaryScaleFixedHistory] as sshf on sshf.SalaryScaleId = ss.Id
		left join SalaryScaleRate as ssr on ssr.SalaryScaleHourlyHistoryId = ssh.Id
		left join SalaryScaleRateSpecification as ssrspec on ssrspec.SalaryScaleRateId = ssr.Id
		where ss.Id = @p1
		order by ssh.FromDate desc, sshf.FromDate desc, ssr.Name asc`, id)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	type row struct {
		id            int32
		name          string
		salarytype    int
		dateh         sql.NullString
		datef         sql.NullString
		fpebyemployee sql.NullFloat64
		fpebyemployer sql.NullFloat64
		defaultrate   sql.NullFloat64
		pebyemployee  sql.NullFloat64
		pebyemployer  sql.NullFloat64
		rateidx       sql.NullInt64
		rateamount    sql.NullFloat64
		rateday       sql.NullString
		ratefrom      sql.NullInt64
		rateto        sql.NullInt64
	}

	var result *salaryscale
	m := make(map[time.Time]*wages.SalaryScaleRevision)
	for rows.Next() {
		r := row{}
		if err := rows.Scan(&r.id, &r.name, &r.salarytype,
			&r.dateh, &r.datef,
			&r.fpebyemployee, &r.fpebyemployer,
			&r.defaultrate, &r.pebyemployee, &r.pebyemployer,
			&r.rateidx, &r.rateamount, &r.rateday, &r.ratefrom, &r.rateto); err != nil {
			return nil, err
		}

		if result == nil {
			result = &salaryscale{
				Entity: &wages.SalaryScale{
					ID:         r.id,
					Name:       r.name,
					SalaryType: wages.SalaryType(r.salarytype),
				},
			}
		}

		var date time.Time
		if r.dateh.Valid {
			date, err = time.Parse(time.RFC3339, r.dateh.String)
			if err != nil {
				return nil, err
			}
		}
		if r.datef.Valid {
			date, err = time.Parse(time.RFC3339, r.datef.String)
			if err != nil {
				return nil, err
			}
		}
		rev, ok := m[date]
		if !ok {
			rev = &wages.SalaryScaleRevision{Date: date}
			if r.defaultrate.Valid {
				rev.DefaultRate = new(amount.DKK).SetFloat64(r.defaultrate.Float64)
			}
			if r.fpebyemployee.Valid || r.fpebyemployer.Valid {
				rev.Pension = &wages.PensionRates{}
				if r.fpebyemployee.Valid {
					rev.Pension.RateByEmployee = new(amount.Percentage).SetFloat64(r.fpebyemployee.Float64 / 100.0)
				}
				if r.fpebyemployer.Valid {
					rev.Pension.RateByEmployer = new(amount.Percentage).SetFloat64(r.fpebyemployer.Float64 / 100.0)
				}
			}
			if r.pebyemployee.Valid || r.pebyemployer.Valid {
				rev.Pension = &wages.PensionRates{}
				if r.pebyemployee.Valid {
					rev.Pension.RateByEmployee = new(amount.Percentage).SetFloat64(r.pebyemployee.Float64 / 100.0)
				}
				if r.pebyemployer.Valid {
					rev.Pension.RateByEmployer = new(amount.Percentage).SetFloat64(r.pebyemployer.Float64 / 100.0)
				}
			}

			m[date] = rev
			result.Revisions = append(result.Revisions, rev)
		}

		if r.rateidx.Valid {
			rate := wages.SalaryScaleRate{
				Index:      int(r.rateidx.Int64),
				Amount:     new(amount.DKK).SetFloat64(r.rateamount.Float64),
				PeriodFrom: int(r.ratefrom.Int64),
				PeriodTo:   int(r.rateto.Int64),
			}

			day, err := strconv.ParseUint(r.rateday.String, 10, 64)
			if err != nil {
				return nil, err
			}
			switch day {
			case 6:
				rate.WeekDays = wages.WeekdaySunday
			case 7:
				rate.WeekDays = wages.WeekdayMonday | wages.WeekdayTuesday | wages.WeekdayWednesday | wages.WeekdayThursday | wages.WeekdayFriday
			default:
				rate.WeekDays = 1 << day
			}

			rev.Rates = append(rev.Rates, &rate)
		}

	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return result, nil
}
