package query

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/stjarni/wages"
)

func (q *query) EmployeeShifts(ctx context.Context, p *wages.Period, empls ...*wages.Employee) (wages.EmployeeShifts, error) {
	query := `select ut.UserId, ut.Id, ut.CheckinTime, ut.CheckoutTime,
	  ut.DepartmentId, ut.TaskId
	from [UserTask] as ut
	inner join [Approvals] as a on a.UserTaskId = ut.Id
	where a.Status = @p1 and ut.CheckinTime >= @p2 and ut.CheckoutTime <= @p3`

	args := []interface{}{1, p.From, p.To}
	if len(empls) != 0 {
		var params []string
		for idx, empl := range empls {
			args = append(args, empl.ID)
			params = append(params, fmt.Sprintf("@p%d", idx+4))
		}
		query += fmt.Sprintf(" and ut.UserId in (%s)", strings.Join(params, ","))
	}
	query += "\norder by ut.CheckoutTime desc"

	rows, err := q.db.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	var emplID int
	result := make(wages.EmployeeShifts)
	for rows.Next() {
		var shift wages.EmployeeShift
		if err := rows.Scan(&emplID, &shift.ID, &shift.Checkin, &shift.Checkout, &shift.DepartmentID, &shift.TaskID); err != nil {
			return nil, err
		}
		result[emplID] = append(result[emplID], &shift)
	}
	if rows.Err() != nil {
		return nil, err
	}
	return result, nil
}
