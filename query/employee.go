package query

import (
	"context"
	"database/sql"
	"fmt"
	"sort"
	"strings"
	"time"

	"gitlab.com/stjarni/wages"
	"gitlab.com/stjarni/wages/core/amount"
	"gitlab.com/stjarni/wages/core/tracker"
)

type empl struct {
	ID        int
	Name      string
	Ptal      string
	Gender    wages.Gender
	BirthDate time.Time
	Email     string
	Address   string
	Phone     string
	Revisions emplrevs
}

type emplrev struct {
	Date              time.Time
	MonthySalary      *amount.DKK
	SalaryAccount     *wages.BankAccount
	Pension           *wages.PensionRates
	PensionAccount    *wages.BankAccount
	UnionMembershipNo string
	UnionID           int32
	LocalDepartmentID int32
	PensionCompanyID  int32
	Positions         []*emplrevpos
}

type emplrevpos struct {
	DepartmentID     int32
	Task             *wages.EmployeeTask
	SalaryScaleID    int32
	SplitRatio       *amount.Percentage
	LunchBreakPolicy *wages.LunchBreakPolicy
}

type emplrevs []*emplrev

func (revs *emplrevs) period(period *wages.Period) []*emplrev {
	var result []*emplrev
	for _, r := range *revs {
		if r.Date.After(period.To) || r.Date.Equal(period.To) {
			continue
		}
		if r.Date.After(period.From) {
			result = append(result, r)
		} else {
			result = append(result, r)
			break
		}
	}
	return result
}

func (revs *emplrevs) date(date time.Time) *emplrev {
	for _, r := range *revs {
		if r.Date.After(date) {
			continue
		} else {
			return r
		}
	}
	return nil
}

func (q *query) employees(ctx context.Context, ids ...int32) ([]*empl, error) {
	query := `select e.Id, e.Email, e.FirstName, e.LastName, e.MiddleName,
	  e.Ptal, e.StreetAndNo, e.PostCodeAndCity, e.DateOfBirth, e.IsMale, e.MobilePhone,
	  eh.FromDate,
	  eh.UnionMemberNo, eh.UnionId, eh.LocalDepartmentId, eh.PensionCompanyId, 
	  eh.PensionRateByEmployee, eh.PensionRateByEmployer,
	  eh.PensionAccount, pbank.RegNo, pbank.Name, 
	  eh.MonthlySalary, eh.Account, sbank.RegNo, sbank.Name,
	  epos.DepartmentId, et.Id, et.Name, epos.SalaryScaleId,
	  epos.FixedSalaryPartPercentages,
	  epos.LunchBreakMinutesPerHours, epos.MaxHoursWithoutLunchBreak, epos.MaxLunchBreakReductionMinutes
	from [User] as e
	inner join [UserHistory] as eh on eh.UserId = e.Id
	left join [UserDepartmentDetails] as epos on epos.UserHistoryId = eh.Id
	left join [Task] as et on et.Id = epos.TaskId
	left join [Bank] as sbank on sbank.RegNo = eh.AccountBankId
	left join [Bank] as pbank on pbank.RegNo = eh.PensionBankId`

	var args []interface{}
	if len(ids) != 0 {
		var params []string
		for idx, id := range ids {
			args = append(args, id)
			params = append(params, fmt.Sprintf("@p%d", idx+1))
		}
		query += fmt.Sprintf("\nwhere e.Id in (%s)", strings.Join(params, ","))
	}

	query += "\norder by e.Id asc, eh.FromDate desc, epos.ID asc"

	rows, err := q.db.QueryContext(ctx, query, args...)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	type row struct {
		id            int
		firstName     string
		lastName      string
		middleName    sql.NullString
		email         string
		phone         sql.NullString
		ptal          sql.NullString
		address1      sql.NullString
		address2      sql.NullString
		birthdate     sql.NullString
		ismale        bool
		date          time.Time
		umemberno     sql.NullString
		uid           sql.NullInt64
		ldid          sql.NullInt64
		pencompanyid  sql.NullInt64
		penbyemployee sql.NullFloat64
		penbyemployer sql.NullFloat64
		penaccount    sql.NullString
		penbankregno  sql.NullString
		penbankname   sql.NullString
		salary        sql.NullFloat64
		saccount      sql.NullString
		sbankregno    sql.NullString
		sbankname     sql.NullString
		depid         sql.NullInt64
		taskid        sql.NullInt64
		taskname      sql.NullString
		sscaleid      sql.NullInt64
		splitratio    sql.NullFloat64
		lbrate        sql.NullInt64
		lbthreshold   sql.NullInt64
		lbmax         sql.NullInt64
	}

	empls := make(map[int]*empl)
	emplrevs := make(map[time.Time]*emplrev)
	var result []*empl

	for rows.Next() {
		r := row{}

		if err := rows.Scan(&r.id,
			&r.email, &r.firstName, &r.lastName, &r.middleName,
			&r.ptal, &r.address1, &r.address2, &r.birthdate, &r.ismale, &r.phone,
			&r.date, &r.umemberno, &r.uid, &r.ldid, &r.pencompanyid,
			&r.penbyemployee, &r.penbyemployer, &r.penaccount, &r.penbankregno, &r.penbankname,
			&r.salary, &r.saccount, &r.sbankregno, &r.sbankname,
			&r.depid, &r.taskid, &r.taskname, &r.sscaleid,
			&r.splitratio,
			&r.lbrate, &r.lbthreshold, &r.lbmax); err != nil {
			return nil, err
		}
		e, ok := empls[r.id]
		if !ok {
			emplrevs = make(map[time.Time]*emplrev) // reset revisions map

			var name string
			if r.middleName.Valid {
				name = strings.Join([]string{r.firstName, r.middleName.String, r.lastName}, " ")
			} else {
				name = strings.Join([]string{r.firstName, r.lastName}, " ")
			}
			e = &empl{ID: r.id, Name: name, Email: r.email}
			e.Email = r.email
			if r.ismale {
				e.Gender = wages.GenderMale
			} else {
				e.Gender = wages.GenderFemale
			}
			if r.ptal.Valid {
				e.Ptal = r.ptal.String
			}
			if r.phone.Valid {
				e.Phone = r.phone.String
			}

			if r.address1.Valid && r.address2.Valid {
				e.Address = strings.Join([]string{r.address1.String, r.address2.String}, " ")
			} else if r.address1.Valid {
				e.Address = r.address1.String

			} else if r.address2.Valid {
				e.Address = r.address2.String
			}

			if r.birthdate.Valid {
				if birthdate, err := time.Parse(time.RFC3339, r.birthdate.String); err != nil {
					return nil, err
				} else {
					e.BirthDate = birthdate
				}
			}
			empls[r.id] = e
			result = append(result, e)
		}

		rev, ok := emplrevs[r.date]
		if !ok {
			rev = &emplrev{
				Date: r.date,
			}
			if r.salary.Valid {
				rev.MonthySalary = new(amount.DKK).SetFloat64(r.salary.Float64)
			}
			if r.sbankregno.Valid {
				rev.SalaryAccount = &wages.BankAccount{
					AccountNo: r.saccount.String,
					RegNo:     r.sbankregno.String,
					BankName:  r.sbankname.String,
				}
			}
			if r.penbyemployee.Valid || r.penbyemployer.Valid {
				rev.Pension = &wages.PensionRates{}
				if r.penbyemployee.Valid {
					rev.Pension.RateByEmployee = new(amount.Percentage).SetFloat64(r.penbyemployee.Float64 / 100.0)
				}
				if r.penbyemployer.Valid {
					rev.Pension.RateByEmployer = new(amount.Percentage).SetFloat64(r.penbyemployer.Float64 / 100.0)
				}
			}
			if r.penbankregno.Valid {
				rev.PensionAccount = &wages.BankAccount{
					AccountNo: r.penaccount.String,
					RegNo:     r.penbankregno.String,
					BankName:  r.penbankname.String,
				}
			}
			if r.umemberno.Valid {
				rev.UnionMembershipNo = r.umemberno.String
			}
			if r.uid.Valid {
				rev.UnionID = int32(r.uid.Int64)
			}
			if r.ldid.Valid {
				rev.LocalDepartmentID = int32(r.ldid.Int64)
			}
			if r.pencompanyid.Valid {
				rev.PensionCompanyID = int32(r.pencompanyid.Int64)
			}
			e.Revisions = append(e.Revisions, rev)
			emplrevs[r.date] = rev
		}

		if r.depid.Valid && r.taskid.Valid && r.sscaleid.Valid {
			pos := emplrevpos{
				DepartmentID: int32(r.depid.Int64),
				Task: &wages.EmployeeTask{
					ID:   int32(r.taskid.Int64),
					Name: r.taskname.String,
				},
				SalaryScaleID: int32(r.sscaleid.Int64),
			}
			if r.splitratio.Valid {
				pos.SplitRatio = new(amount.Percentage).SetFloat64(r.splitratio.Float64 / 100.0)
			} else {
				pos.SplitRatio = new(amount.Percentage).SetFloat64(1.0)
			}
			if r.lbrate.Valid && r.lbthreshold.Valid {
				pos.LunchBreakPolicy = &wages.LunchBreakPolicy{
					ReductionRate:      r.lbrate.Int64,
					ReductionThreshold: time.Duration(time.Hour * time.Duration(r.lbthreshold.Int64)),
				}
				if r.lbmax.Valid {
					pos.LunchBreakPolicy.ReductionMax = time.Duration(time.Minute * time.Duration(r.lbmax.Int64))
				}
			}
			rev.Positions = append(rev.Positions, &pos)
		}
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return result, nil
}

func (q *query) emplrevisions(ctx context.Context, bonuses emplbonuses, empls []*empl, p *wages.Period) ([]*wages.Employee, error) {
	var result []*wages.Employee
	for _, empl := range empls {
		e := &wages.Employee{
			ID:        empl.ID,
			Name:      empl.Name,
			Email:     empl.Email,
			Ptal:      empl.Ptal,
			Gender:    empl.Gender,
			BirthDate: empl.BirthDate,
			Phone:     empl.Phone,
			Address:   empl.Address,
		}

		departments, trackDepartment := tracker.New(p)
		salaryscales, trackSalaryScale := tracker.New(p)
		unions, trackUnion := tracker.New(p)
		localdeps, trackLocalDep := tracker.New(p)
		dateset := make(map[time.Time]struct{})
		dateset[p.From] = struct{}{}
		for _, r := range empl.Revisions.period(p) {
			if r.Date.After(p.From) {
				dateset[r.Date] = struct{}{}
			}

			trackUnion(r.UnionID, r.Date)
			trackLocalDep(r.LocalDepartmentID, r.Date)
			for _, p := range r.Positions {
				trackDepartment(p.DepartmentID, r.Date)
				trackSalaryScale(p.SalaryScaleID, r.Date)
			}
		}

		for _, tr := range *salaryscales {
			ss, ok := q.salaryscales[tr.EntityID]
			if !ok {
				var err error
				ss, err = q.salaryscale(ctx, tr.EntityID)
				if err != nil {
					return nil, err
				}
				q.salaryscales[tr.EntityID] = ss
			}

			for _, r := range ss.Revisions.period(p) {
				dateset[r.Date] = struct{}{}
			}
		}

		for _, tr := range *departments {
			dep, ok := q.departments[tr.EntityID]
			if !ok {
				var err error
				dep, err = q.department(ctx, tr.EntityID)
				if err != nil {
					return nil, err
				}
				q.departments[tr.EntityID] = dep
			}

			for _, r := range dep.revisions.period(p) {
				dateset[r.Date] = struct{}{}
			}
		}

		for _, tr := range *unions {
			u, ok := q.unions[tr.EntityID]
			if !ok {
				var err error
				u, err = q.union(ctx, tr.EntityID)
				if err != nil {
					return nil, err
				}
				q.unions[tr.EntityID] = u
			}

			for _, r := range u.revisions.period(p) {
				dateset[r.Date] = struct{}{}
			}
		}

		for _, tr := range *localdeps {
			ld, ok := q.localdeps[tr.EntityID]
			if !ok {
				var err error
				ld, err = q.localdep(ctx, tr.EntityID)
				if err != nil {
					return nil, err
				}
				q.localdeps[tr.EntityID] = ld
			}

			for _, r := range ld.revisions.period(p) {
				dateset[r.Date] = struct{}{}
			}
		}

		for _, b := range bonuses[empl.ID] {
			if !b.StartDate.Before(p.From) {
				dateset[b.StartDate] = struct{}{}
			}
			if !b.EndDate.IsZero() && b.EndDate.Before(p.To) {
				dateset[b.EndDate.Add(time.Duration(24*time.Hour))] = struct{}{}
			}
		}

		var dates []time.Time
		for d := range dateset {
			dates = append(dates, d)
		}

		sort.Slice(dates, func(i, j int) bool {
			return dates[i].After(dates[j])
		})

		for _, d := range dates {
			emplrev := empl.Revisions.date(d)
			if emplrev == nil {
				continue
			}

			rev := &wages.EmployeeRevision{
				Date:              d,
				MonthySalary:      emplrev.MonthySalary,
				SalaryAccount:     emplrev.SalaryAccount,
				Pension:           emplrev.Pension,
				PensionAccount:    emplrev.PensionAccount,
				UnionMembershipNo: emplrev.UnionMembershipNo,
				Positions:         make(wages.EmployeePositions),
			}

			unionid := emplrev.UnionID
			if emplrev.LocalDepartmentID != 0 {
				ld := q.localdeps[emplrev.LocalDepartmentID]
				ldrev := ld.revisions.date(d)
				if ldrev == nil {
					return nil, fmt.Errorf("unable to locate local department revision for %d for %s", emplrev.LocalDepartmentID, d)
				}

				// set employee union to local department union
				unionid = ldrev.UnionID

				rev.LocalDepartment = &wages.EmployeeLocalDepartmentRevision{
					ID:                      ld.entity.ID,
					Name:                    ld.entity.Name,
					Date:                    ldrev.Date,
					MembershipFee:           ldrev.MembershipFee,
					MembershipFeeAccount:    ldrev.MembershipFeeAccount,
					HolidayAllowanceAccount: ldrev.HolidayAllowanceAccount,
				}
			}

			// if employee has union or has localdep which has a union
			if unionid != 0 {
				u, ok := q.unions[unionid]
				if !ok {
					var err error
					u, err = q.union(ctx, unionid)
					if err != nil {
						return nil, err
					}
					q.unions[unionid] = u
				}
				urev := u.revisions.date(d)
				if urev == nil {
					return nil, fmt.Errorf("unable to locate union revision for %d for %s", unionid, d)
				}
				rev.Union = &wages.EmployeeUnionRevision{
					ID:                      u.entity.ID,
					Name:                    u.entity.Name,
					Email:                   u.entity.Email,
					Phone:                   u.entity.Phone,
					Date:                    urev.Date,
					MembershipFee:           urev.MembershipFee,
					InsuranceFee:            urev.InsuranceFee,
					Pension:                 urev.Pension,
					MembershipFeeAccount:    urev.MembershipFeeAccount,
					InsuranceFeeAccount:     urev.InsuranceFeeAccount,
					PensionAccount:          urev.PensionAccount,
					HolidayAllowanceAccount: urev.HolidayAllowanceAccount,
				}
			}

			for _, emplpos := range emplrev.Positions {
				dep := q.departments[emplpos.DepartmentID]
				deprev := dep.revisions.date(d)
				var lb *wages.LunchBreakPolicy
				if emplpos.LunchBreakPolicy != nil {
					lb = emplpos.LunchBreakPolicy
				} else if deprev != nil && deprev.LunchBreakPolicy != nil {
					lb = deprev.LunchBreakPolicy
				}

				ss := q.salaryscales[emplpos.SalaryScaleID]
				ssrev := ss.Revisions.date(d)
				pos := &wages.EmployeePosition{
					Department: dep.entity,
					Task:       emplpos.Task,
					SalaryScale: &wages.EmployeeSalaryScale{
						ID:          ss.Entity.ID,
						Name:        ss.Entity.Name,
						SalaryType:  ss.Entity.SalaryType,
						Date:        ssrev.Date,
						DefaultRate: ssrev.DefaultRate,
						Pension:     ssrev.Pension,
						Rates:       ssrev.Rates,
					},
					SplitRatio:       emplpos.SplitRatio,
					LunchBreakPolicy: lb,
				}

				// collect bonus for current posittion for date 'd'
				// TODO: awful enumeration
				for _, b := range bonuses[empl.ID] {
					if !b.StartDate.After(d) && (b.EndDate.IsZero() || !b.EndDate.Before(d)) &&
						b.DepartmentID == emplpos.DepartmentID && b.TaskID == emplpos.Task.ID {
						pos.Bonuses = append(pos.Bonuses, b)
					}
				}

				rev.Positions.SetPosition(emplpos.DepartmentID, emplpos.Task.ID, pos)
			}

			e.Revisions = append(e.Revisions, rev)
		}

		if len(e.Revisions) != 0 {
			result = append(result, e)
		}
	}

	return result, nil
}
