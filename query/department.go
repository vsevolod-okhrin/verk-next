package query

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"gitlab.com/stjarni/wages"
)

type department struct {
	entity    *wages.Department
	revisions deprevs
}

type deprev struct {
	Date             time.Time
	LunchBreakPolicy *wages.LunchBreakPolicy
}

type deprevs []*deprev

func (revs *deprevs) period(period *wages.Period) []*deprev {
	var result []*deprev
	for _, r := range *revs {
		if r.Date.After(period.To) || r.Date.Equal(period.To) {
			continue
		}
		if r.Date.After(period.From) {
			result = append(result, r)
		} else {
			result = append(result, r)
			break
		}
	}
	return result
}

func (revs *deprevs) date(date time.Time) *deprev {
	for _, r := range *revs {
		if r.Date.After(date) {
			continue
		} else {
			return r
		}
	}
	return nil
}

func (q *query) department(ctx context.Context, id int32) (*department, error) {
	rows, err := q.db.QueryContext(ctx, `select d.Id, d.Name, p.Id, p.Name, dh.FromDate, 
		dh.LunchBreakMinutesPerHours, dh.MaxHoursWithoutLunchBreak, dh.MaxLunchBreakReductionMinutes
		from [Department] as d
		inner join [DepartmentHistory] as dh on dh.DepartmentId = d.Id
		left join [Department] as p on p.Id = d.ParentDepartmentId
		where d.Id = @p1
		order by dh.FromDate desc
	`, id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	type row struct {
		id          int32
		name        string
		parentid    sql.NullInt64
		parentname  sql.NullString
		date        time.Time
		lbrate      sql.NullInt64
		lbthreshold sql.NullInt64
		lbmax       sql.NullInt64
	}

	var result *department
	for rows.Next() {
		r := row{}
		if err := rows.Scan(&r.id, &r.name, &r.parentid, &r.parentname, &r.date,
			&r.lbrate, &r.lbthreshold, &r.lbmax); err != nil {
			return nil, err
		}

		if result == nil {
			result = &department{
				entity: &wages.Department{ID: r.id, Name: r.name},
			}

			if r.parentid.Valid {
				parent := wages.Department{ID: int32(r.parentid.Int64), Name: r.parentname.String}
				result.entity.HeadDepartment = &parent
				result.entity.Name = fmt.Sprintf("%s - %s", parent.Name, result.entity.Name)
			}
		}

		rev := &deprev{Date: r.date}
		if r.lbrate.Valid && r.lbthreshold.Valid {
			rev.LunchBreakPolicy = &wages.LunchBreakPolicy{
				ReductionRate:      r.lbrate.Int64,
				ReductionThreshold: time.Duration(time.Hour * time.Duration(r.lbthreshold.Int64)),
			}
			if r.lbmax.Valid {
				rev.LunchBreakPolicy.ReductionMax = time.Duration(time.Minute * time.Duration(r.lbmax.Int64))
			}
		}
		result.revisions = append(result.revisions, rev)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return result, nil
}
