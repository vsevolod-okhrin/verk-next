package timeutil

import "time"

// StartOf returns beginning of day t
func StartOf(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location())
}

// Date returns day
func Date(year int, month time.Month, day int) time.Time {
	return time.Date(year, month, day, 0, 0, 0, 0, time.UTC)
}

func DurationInSeconds(nsecs int64) time.Duration {
	return time.Duration(nsecs) / time.Second
}
