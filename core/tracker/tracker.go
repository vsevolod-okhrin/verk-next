package tracker

import (
	"time"

	"gitlab.com/stjarni/wages"
)

// TrackRecord ...
type TrackRecord struct {
	EntityID int32
	Period   *wages.Period
}

// New creates records and tracker function
func New(period *wages.Period) (*[]TrackRecord, func(id int32, revision time.Time)) {
	var i *TrackRecord
	var records []TrackRecord

	return &records, func(id int32, revision time.Time) {
		// localfunc: create new track record and push to result if entity is not nil
		createrecord := func(from time.Time, to time.Time) {
			p := &wages.Period{From: from, To: to}
			if period.From.Before(period.From) {
				p.From = period.From
			}
			i = &TrackRecord{EntityID: id, Period: p}
			if id != 0 {
				records = append(records, *i)
			}
		}

		// if nil bootstrap i
		if i == nil {
			createrecord(revision, period.To)
		} else {
			if i.EntityID == id {
				i.Period.From = revision
			} else {
				createrecord(revision, i.Period.From)
			}
		}
	}
}
