package amount

// Amount type
type Amount int

const (
	// AmountDKK is a fixed amount in Danish Krones
	AmountDKK Amount = 0x1
	// AmountPercentage is a percentage amount
	AmountPercentage = 0x2
)
