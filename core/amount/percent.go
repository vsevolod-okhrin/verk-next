package amount

import (
	"encoding/json"
	"fmt"
	"math"
	"math/big"
)

// Precision is 1/100000 percent
const Precision = 100000

// Percentage is a fraction of 100000
type Percentage struct {
	v int64
}

// SetFloat64 sets z to rounded value of f (ex: 0.015 -> 1500; 0.00125 -> 125)
func (z *Percentage) SetFloat64(f float64) *Percentage {
	z.v = int64(math.Floor(f * Precision))
	return z
}

// Rat returns rational represenation of fraction
func (z *Percentage) Rat() *big.Rat {
	return new(big.Rat).SetFrac64(z.v, Precision)
}

func (z *Percentage) String() string {
	return fmt.Sprintf("%.1f %%", float64(z.v)/Precision*100.0)
}

// MarshalJSON implements json Marshaler
func (z *Percentage) MarshalJSON() ([]byte, error) {
	return json.Marshal(z.String())
}
