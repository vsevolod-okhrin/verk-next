package amount

import (
	"encoding/json"
	"math"
	"math/big"

	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

// DKK is Danish Krone currency
type DKK struct {
	øre int64
}

// NewDKK creates new DKK amount from krone and øre values
func NewDKK(krone int64, øre int64) *DKK {
	return &DKK{øre: krone*100 + øre}
}

// SetFloat64 sets z to the rounded value of x
func (z *DKK) SetFloat64(x float64) *DKK {
	krone, øre := math.Modf(x)
	z.øre = int64(krone)*100 + int64(math.Round(øre*100.0))
	return z
}

// SetRat sets z to the rounded value of x
func (z *DKK) SetRat(x *big.Rat) *DKK {
	f64, _ := x.Float64()
	return z.SetFloat64(f64)
}

// Int64 returns amount of øres for z
func (z *DKK) Int64() int64 {
	return z.øre
}

// Add ...
func (z *DKK) Add(x, y *DKK) *DKK {
	z.øre = (x.øre + y.øre)
	return z
}

// Inc ...
func (z *DKK) Inc(x *DKK) {
	z.øre += x.øre
}

// Sub ...
func (z *DKK) Sub(x, y *DKK) *DKK {
	z.øre = (x.øre - y.øre)
	return z
}

// MarshalJSON implements Marshaler
func (z *DKK) MarshalJSON() ([]byte, error) {
	return json.Marshal(z.String())
}

func (z *DKK) String() string {
	p := message.NewPrinter(language.Danish)
	krone := z.øre / 100
	øre := z.øre % 100
	return p.Sprintf("%d,%02d", krone, øre)
}
