package timeline

import (
	"fmt"
	"time"

	"gitlab.com/stjarni/wages"
)

// Timeline ...
type Timeline struct {
	date         time.Time // what for?
	policies     []*wages.CompanyPolicy
	revisions    []*wages.EmployeeRevision
	policiesidx  int
	revisionsidx int
}

// New creates new timeline
func New(policies []*wages.CompanyPolicy, revisions []*wages.EmployeeRevision) *Timeline {
	t := Timeline{policies: policies, revisions: revisions}
	return &t
}

// Date returns current date
func (t *Timeline) Date() time.Time {
	return t.date
}

// CompanyPolicy returns current company policy (actual for timeline date)
func (t *Timeline) CompanyPolicy() *wages.CompanyPolicy {
	return t.policies[t.policiesidx]
}

// EmployeeRevision returns actual employee revision (actual for timeline date)
func (t *Timeline) EmployeeRevision() *wages.EmployeeRevision {
	if t.revisionsidx == len(t.revisions) {
		return nil
	}
	return t.revisions[t.revisionsidx]
}

// Reset revisions to last
func (t *Timeline) Reset(date time.Time) {
	t.date = date
	t.policiesidx = 0
	t.revisionsidx = 0
}

// Rewind timeline backwards in time
func (t *Timeline) Rewind(date time.Time) {
	if date.After(t.date) {
		panic("timeline: can only rewind backwards")
	}
	t.date = date

	for t.revisionsidx < len(t.revisions) && date.Before(t.EmployeeRevision().Date) {
		t.revisionsidx++
	}

	for date.Before(t.CompanyPolicy().Date) {
		t.policiesidx++
		if t.policiesidx >= len(t.policies) {
			panic("timeline: company policies out of range")
		}
	}
}

// RewindDay rewinds timeline backwards one day
func (t *Timeline) RewindDay() {
	t.date.Add(-24 * time.Hour)

	if t.date.Before(t.EmployeeRevision().Date) {
		if t.revisionsidx > len(t.revisions) {
			panic(fmt.Errorf("timeline: revisions out of range: date %v", t.date))
		}
		t.revisionsidx++
	}
	if t.date.Before(t.CompanyPolicy().Date) {
		if t.policiesidx > len(t.policies) {
			panic("timeline: company policies out of range")
		}
		t.policiesidx++
	}
}
