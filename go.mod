module gitlab.com/stjarni/wages

require (
	cloud.google.com/go v0.24.0
	github.com/denisenkom/go-mssqldb v0.0.0-20180625034930-3724b4745ca9
	github.com/golang/protobuf v1.1.0
	golang.org/x/crypto v0.0.0-20180621125126-a49355c7e3f8
	golang.org/x/net v0.0.0-20180621144259-afe8f62b1d6b
	golang.org/x/text v0.3.0
	google.golang.org/genproto v0.0.0-20180621235812-80063a038e33
	google.golang.org/grpc v1.13.0
)
