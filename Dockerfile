FROM golang:1.10 as builder

RUN mkdir -p /go/src/gitlab.com/stjarni/wages
WORKDIR /go/src/gitlab.com/stjarni/wages
ADD . .

RUN go get -u golang.org/x/vgo
RUN CGO_ENABLED=0 GOOS=linux vgo build -a -o app cmd/wages/main.go

FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder /go/src/gitlab.com/stjarni/wages/app .

ENV WAGES_ADDR=0.0.0.0
ENV WAGES_PORT=50052

EXPOSE 50052

CMD ["./app"]